package com.rabdorms.forcemobilestudios.cell;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.rabdorms.forcemobilestudios.R;


public class ContactInfoCell extends ConstraintLayout {

    TextView title_label;
    TextView phone_label;
    ImageView imageView;

    public ContactInfoCell(Context context) {
        super(context);
        init();
    }

    public ContactInfoCell(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ContactInfoCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        inflate(getContext(), R.layout.cell_contact_info, this);
        title_label = (TextView) findViewById(R.id.title_label);
        phone_label = (TextView) findViewById(R.id.phone_label);
        imageView = (ImageView)findViewById(R.id.image_view);
    }

    public void setTitleAndPhoneAndImage(String title, String phone, int resid) {
        title_label.setText(title);
        phone_label.setText(phone);
        imageView.setImageResource(resid);
    }

}
