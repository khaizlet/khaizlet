package com.rabdorms.forcemobilestudios.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import com.github.barteksc.pdfviewer.PDFView;
import com.rabdorms.forcemobilestudios.R;

import java.net.MalformedURLException;
import java.net.URL;

public class DormsGuideActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dorms_guide);

        PDFView pdfView = (PDFView) findViewById(R.id.pdfview);

        //pdfView.fromAsset("file:///android_asset/pdf/DormsGuide.pdf").load();
        pdfView.fromAsset("DormsGuide.pdf").load();

    }
}