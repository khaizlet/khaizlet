package com.rabdorms.forcemobilestudios.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rabdorms.forcemobilestudios.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ftac_Fragment extends Fragment implements View.OnClickListener{




    private ProgressDialog dialog;





    public ftac_Fragment()  {
        // Required empty public constructor
//        fetchData process = new fetchData(this);
 //       process.execute();

    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String aboutus = " ";
        TextView ftac_Message;

        final View view = inflater.inflate(R.layout.ftac_layout, container, false);
        TextView text = (TextView) view.findViewById(R.id.ftac_Message);
        fetchData process = new fetchData(this);
        process.execute();

        return view;


    }


    public class fetchData extends AsyncTask<Void, Void, Void> {

            String data = "";
            String aboutus = "";
            String goal = "";
            private ProgressDialog dialog;

           public fetchData(ftac_Fragment ftac_fragment) {
                dialog = new ProgressDialog(getActivity());
            }



            @Override
            protected void onPreExecute() {
                dialog.setMessage("Loading data, please wait...");
                dialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {

                try {
                    URL url = new URL("https://script.google.com/macros/s/AKfycbwpf6hfHBb5vF4e6X8CzNLhDX5YrqQdbO8LzaSnQHutCzAnMgfv/exec");
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String line = "";
                    while (line != null) {
                        line = bufferedReader.readLine();
                        if (line != null)
                            data = data + line;
                    }

                    JSONObject jo_original = new JSONObject(data);
                    JSONArray jsonArray = jo_original.getJSONArray("message");
                    JSONObject jo_each = (JSONObject) jsonArray.get(0);
                    aboutus = jo_each.getString("Message");

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                //getView().findViewById(R.layout.ftac_layout);
                TextView brands = (TextView) getView().findViewById(R.id.ftac_Message);

                brands.setText(aboutus);

            }
        }


        @Override
        public void onClick (View v){

        }



}
