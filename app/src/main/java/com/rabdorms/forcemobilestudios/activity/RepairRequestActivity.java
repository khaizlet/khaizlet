package com.rabdorms.forcemobilestudios.activity;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alex.common.utils.PermissionManager;
import com.alex.common.utils.UIUtil;
import com.rabdorms.forcemobilestudios.R;
import com.shashank.sony.fancytoastlib.FancyToast;
//import com.unstoppable.submitbuttonview.SubmitButton;
import com.alex.common.SubmitButton;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cn.bingoogolapple.alertcontroller.BGAAlertAction;
import cn.bingoogolapple.alertcontroller.BGAAlertController;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


import static com.rabdorms.forcemobilestudios.model.AirmanInfo.g_airman;

public class RepairRequestActivity extends AppCompatActivity {

    TextView name_editor;
    TextView squadron_editor;
    TextView phonenumber_editor;
    TextView building_editor;
    TextView room_editor;

    TextView type_editor;
    TextView desc_editor;

    SegmentedGroup rank_segment;
    SegmentedGroup gender_segment;
    SegmentedGroup base_segment;
    SegmentedGroup urgency_segment;


    private String rank;
    private TextView name;
    private TextView squadron;
    private TextView duty_phone;
    private TextView bldg_num;
    private TextView room_num;
    private TextView urgency;
    private TextView typedesc;
    private TextView desc;
    private String osinput = " Android ";
    private SubmitButton submitButton;
    private Button reportbutton;

    private WebView webView;
    int[] rank_ids = {R.id.Resident, R.id.Supervisor, R.id.Sgt};

    String[] rank_captions = {" ", "AB", "Amn", "A1C", "SrA", "SSgt", "TSgt", "MSgt", "SMSgt", "CMSgt"};



   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



       // for image picker( fixing error : android.os.FileUriExposedException: file:///storage/emulated/0/DCIM/IMG1191059946.jpg exposed beyond app through ClipData.Item.getUri())
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        setContentView(R.layout.activity_repair_request);


       initialize();


       repairrequests();



       if (rank_segment.getCheckedRadioButtonId() == R.id.Sgt) {

          googlesupervisors();

      }else if (rank_segment.getCheckedRadioButtonId() == R.id.Supervisor) {


           googlesupervisors();

      }else {


          googledorms();
      }



   }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_repair_request, menu);
        return true;//super.onCreateOptionsMenu(menu);
    }*/




void googlesupervisors(){




    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://docs.google.com/forms/d/")
            .build();
    final supervisorsheet spreadsheetWebService = retrofit.create(supervisorsheet.class);




    rank_editor = (Spinner) findViewById(R.id.rank_editor);
    name = (TextView) findViewById(R.id.name_editor);
    squadron = (TextView) findViewById(R.id.squadron_editor);
    duty_phone = (TextView) findViewById(R.id.phone_editor);
    base_segment = (SegmentedGroup) findViewById(R.id.base_segment);
    bldg_num = (TextView) findViewById(R.id.buildingnumber_editor);
    room_num = (TextView) findViewById(R.id.roomnumber_editor);
    desc = (TextView) findViewById(R.id.desc_editor);

    submitButton = (SubmitButton) findViewById(R.id.submitbutton);
    submitButton.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                  if (desc_editor.getText().toString().length() == 0) {


                        FancyToast.makeText(getApplicationContext(), "Please enter a Repair Description", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
                        submitButton.doResult(false);

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                submitButton.reset();
                            }
                        }, 3000);





                    }else{




                        submitButton.doResult(true);



                        RadioButton radioButton;
                        radioButton = (RadioButton) findViewById(urgency_segment.getCheckedRadioButtonId());
                        urgency_value = radioButton.getText().toString();
                        radioButton = (RadioButton)findViewById(rank_segment.getCheckedRadioButtonId());
                        rank_value = radioButton.getText().toString();

                        String rankInput = rank_editor.getSelectedItem().toString();
                        String nameInput = name.getText().toString();
                        String squadronInput = squadron.getText().toString();
                        String phoneInput = duty_phone.getText().toString();
                        String baseInput = base_value;
                        String bldgInput = "N/A";
                        String roomInput = "N/A";
                        String urgencyInput = urgency_value;
                        String OSInput = osinput;
                        String statusInput = rank_value;
                        String descInput = desc.getText().toString();


                        Call<Void> completeQuestionnaireCall = spreadsheetWebService.completeQuestionnaire(statusInput,rankInput, nameInput, squadronInput, phoneInput, baseInput, urgencyInput, OSInput, descInput);
                        completeQuestionnaireCall.enqueue(callCallback);


                        for (int i = 0; i < 1; i++) {
                            FancyToast.makeText(getApplicationContext(), "Repair Request Posted to Database!", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();
                        }

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {

                                submitRequest();
                            }
                        }, 3000);


                    }
                }

            });










}


void repairrequests() {

    reportbutton = (Button) findViewById(R.id.report);
    reportbutton.setOnClickListener(
    new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent;
            if(v.equals(reportbutton)) {


                intent = new Intent(RepairRequestActivity.this, repairResource.class);
                startActivity(intent);
              //  startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://docs.google.com/spreadsheets/d/e/2PACX-1vSOLApUREC-4DJas_wwKNylPtveZE_BpQGJzNHeW9UttlFl_nLnFhKCWEF-YtUriah3T4R84Jd2HvbP/pubhtml?gid=685620465&single=true")));






                //  public int topNavigationView;

              /*  setContentView(R.layout.repair_webview);


                    final WebView webView = (WebView) findViewById(R.id.webView1);
                    webView.setInitialScale(1);
                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
                    webView.getSettings().setSupportMultipleWindows(false);
                    webView.getSettings().setSupportZoom(false);
                    webView.setVerticalScrollBarEnabled(false);
                    webView.setHorizontalScrollBarEnabled(false);
                    webView.getSettings().setUserAgentString("Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3");
                    webView.getSettings().setUseWideViewPort(true);
                    webView.getSettings().setLoadWithOverviewMode(true);
                    webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
                    webView.setScrollbarFadingEnabled(false);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
                    }

                    webView.setWebViewClient(new WebViewClient());
                    webView.loadUrl("https://docs.google.com/spreadsheets/d/e/2PACX-1vSOLApUREC-4DJas_wwKNylPtveZE_BpQGJzNHeW9UttlFl_nLnFhKCWEF-YtUriah3T4R84Jd2HvbP/pubhtml?gid=685620465&single=true");


*/











                }
        }
    });


}




void googledorms(){



    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://docs.google.com/forms/d/")
            .build();
    final questionsheetservice spreadsheetWebService = retrofit.create(questionsheetservice.class);




    rank_editor = (Spinner) findViewById(R.id.rank_editor);
    name = (TextView) findViewById(R.id.name_editor);
    squadron = (TextView) findViewById(R.id.squadron_editor);
    duty_phone = (TextView) findViewById(R.id.phone_editor);
    base_segment = (SegmentedGroup) findViewById(R.id.base_segment);
    bldg_num = (TextView) findViewById(R.id.buildingnumber_editor);
    room_num = (TextView) findViewById(R.id.roomnumber_editor);
    desc = (TextView) findViewById(R.id.desc_editor);

    submitButton = (SubmitButton) findViewById(R.id.submitbutton);
    submitButton.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                 if (desc_editor.getText().toString().length() == 0) {


                        FancyToast.makeText(getApplicationContext(), "Please enter a Repair Description", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
                        submitButton.doResult(false);

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                submitButton.reset();
                            }
                        }, 3000);





                    }else{




                        submitButton.doResult(true);



                        RadioButton radioButton;
                        radioButton = (RadioButton) findViewById(urgency_segment.getCheckedRadioButtonId());
                        urgency_value = radioButton.getText().toString();
                        radioButton = (RadioButton)findViewById(rank_segment.getCheckedRadioButtonId());
                        rank_value = radioButton.getText().toString();

                        String rankInput = rank_editor.getSelectedItem().toString();
                        String nameInput = name.getText().toString();
                        String squadronInput = squadron.getText().toString();
                        String phoneInput = duty_phone.getText().toString();
                        String baseInput = base_value;
                        String bldgInput = bldg_num.getText().toString();
                        String roomInput = room_num.getText().toString();
                        String urgencyInput = urgency_value;
                        String OSInput = osinput;
                        String statusInput = rank_value;
                        String descInput = desc.getText().toString();


                        Call<Void> completeQuestionnaireCall = spreadsheetWebService.completeQuestionnaire( statusInput,rankInput, nameInput, squadronInput, phoneInput, baseInput, bldgInput, roomInput, urgencyInput, OSInput, descInput);
                        completeQuestionnaireCall.enqueue(callCallback);


                        for (int i = 0; i < 1; i++) {
                            FancyToast.makeText(getApplicationContext(), "Repair Request Posted to Database!", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();
                        }

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {

                                submitRequest();
                            }
                        }, 3000);


                    }
                }

            });










}













    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();




        if (id == R.id.repair_request_submit) {

            submitRequest();



        }

        return true;
    }

    // class variables
    private static final int REQUEST_CODE = 123;
    private ArrayList<String> mResults = new ArrayList<>();

    void selectImages() {

        if (PermissionManager.isStorageAllowed(this) && PermissionManager.isCameraAllowed(this)) {

            selectImagesWithPermission();

        } else {
            PermissionManager.requestPermission(this, new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
            }, PermissionManager.CODE_CUSTOM_PERM);
        }

    }

    void selectImagesWithPermission() {

// start multiple photos selector
        Intent intent = new Intent(this, ImagesSelectorActivity.class);
// max number of images to be selected
        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 3);
// min size of image which will be shown; to filter tiny images (mainly icons)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100000);
// show camera or not
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
// pass current selected images as the initial value
        intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
// start the selector
        startActivityForResult(intent, REQUEST_CODE);
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // get selected images from selector
        if(requestCode == REQUEST_CODE) {
            if(resultCode == RESULT_OK) {
                mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                assert mResults != null;

                ImageView imageView01 = (ImageView)findViewById(R.id.image_view1);
                ImageView imageView02 = (ImageView)findViewById(R.id.image_view2);
                ImageView imageView03 = (ImageView)findViewById(R.id.image_view3);
                TextView desc_view = (TextView)findViewById(R.id.add_image_desc_label);

                imageView01.setVisibility(View.GONE);
                imageView02.setVisibility(View.GONE);
                imageView03.setVisibility(View.GONE);
                desc_view.setVisibility(View.VISIBLE);

                if(mResults.size() > 0) {
                    desc_view.setVisibility(View.GONE);
                    imageView01.setVisibility(View.VISIBLE);
                    imageView01.setImageURI( Uri.parse(mResults.get(0)));
                    if(mResults.size() > 1) {
                        imageView02.setVisibility(View.VISIBLE);
                        imageView02.setImageURI( Uri.parse(mResults.get(1)));
                        if(mResults.size() > 2) {
                            imageView03.setVisibility(View.VISIBLE);
                            imageView03.setImageURI( Uri.parse(mResults.get(2)));
                        }
                    }
                }
            }
        }


        super.onActivityResult(requestCode, resultCode, data);
    }*/


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionManager.CODE_CUSTOM_PERM){
            if (PermissionManager.checkPermissionGrantResult(grantResults)){
                selectImagesWithPermission();

            }
        }
    }

    String rank_value;
    String gender_value;
    String base_value;
    String urgency_value;
    Spinner rank_editor;

    void initialize() {

        LinearLayout domitory_layout = (LinearLayout)findViewById(R.id.dormitory_layout);
        UIUtil.setEnableAllViews(false, domitory_layout);
        //domitory_layout.setFocusable(false);
        LinearLayout personal_layout = (LinearLayout)findViewById(R.id.sgt_layout);
        UIUtil.setEnableAllViews(false, personal_layout);
        //personal_layout.setFocusable(false);

        rank_editor = (Spinner) findViewById(R.id.rank_editor);
        name_editor = (TextView)findViewById(R.id.name_editor);
        squadron_editor = (TextView)findViewById(R.id.squadron_editor);
        phonenumber_editor = (TextView)findViewById(R.id.phone_editor);
        building_editor = (TextView)findViewById(R.id.buildingnumber_editor);
        room_editor = (TextView)findViewById(R.id.roomnumber_editor);

        desc_editor = (TextView)findViewById(R.id.desc_editor);

        rank_segment = (SegmentedGroup)findViewById(R.id.rank_segment);
        gender_segment = (SegmentedGroup)findViewById(R.id.gender_segment);
        base_segment = (SegmentedGroup)findViewById(R.id.base_segment);
        urgency_segment = (SegmentedGroup)findViewById(R.id.urgency_segment);
        int selected_id = rank_segment.getCheckedRadioButtonId();
     //   ((RadioButton)findViewById(R.id.rank_option_ab)).setChecked(true);
        ((RadioButton)findViewById(R.id.gender_option_male)).setChecked(true);
        ((RadioButton)findViewById(R.id.base_option_ramstein)).setChecked(true);
        ((RadioButton)findViewById(R.id.urgency_routine)).setChecked(true);

        if( g_airman == null) return;
        if( g_airman.fname == null) return;

       // airman_label.setText(rank_captions[g_airman.rankedit]);
        //rank = airman_label.getText().toString();

        Spinner spinner = (Spinner) findViewById(R.id.rank_editor);
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(this, R.layout.spinner_style,
                getResources().getStringArray(R.array.Ranks));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(myAdapter);


        rank_editor.setSelection(g_airman.rankedit);

        name_editor.setText(g_airman.fname);

        phonenumber_editor.setText(g_airman.dutyPhone);
        squadron_editor.setText(g_airman.squadron);
        building_editor.setText(g_airman.building);
        room_editor.setText(g_airman.room);

        int[] rank_ids = {R.id.Resident, R.id.Supervisor, R.id.Sgt};
        int[] gender_ids = {R.id.gender_option_male, R.id.gender_option_female};
        int[] base_ids = {R.id.base_option_ramstein, R.id.base_option_kapaun};
        int[] urgency_ids = {R.id.urgency_routine, R.id.urgency_urgent, R.id.urgency_emergency};

        String rankids = rank_ids.toString();


        RadioButton radioButton;
        radioButton = (RadioButton)findViewById(rank_ids[g_airman.rank]);
        radioButton.setChecked(true);
        rank_value = radioButton.getText().toString();

        radioButton = (RadioButton)findViewById(gender_ids[g_airman.gender]);
        radioButton.setChecked(true);
        gender_value = radioButton.getText().toString();

        radioButton = (RadioButton)findViewById(base_ids[g_airman.base]);
        radioButton.setChecked(true);
        base_value = radioButton.getText().toString();

        //urgency_value = "Routine";

        urgency_segment.findViewById(R.id.urgency_emergency).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callEmeregencyADL();
            }
        });

    }






    public static String getCurrentTimeStamp(){
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateTime = dateFormat.format(new Date()); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }














    void submitRequest() {



        rank_editor = (Spinner) findViewById(R.id.rank_editor);


        RadioButton radioButton;
        radioButton = (RadioButton)findViewById(rank_segment.getCheckedRadioButtonId());
        rank_value = radioButton.getText().toString();

        radioButton = (RadioButton)findViewById(gender_segment.getCheckedRadioButtonId());
        gender_value = radioButton.getText().toString();

        radioButton = (RadioButton)findViewById(base_segment.getCheckedRadioButtonId());
        base_value = radioButton.getText().toString();

        radioButton = (RadioButton)findViewById(urgency_segment.getCheckedRadioButtonId());
        urgency_value = radioButton.getText().toString();


        String subject = base_value + " - " + urgency_value + " - Bldg: " + building_editor.getText().toString() + " Room: " + room_editor.getText().toString();

        String body = " Status: " + rank_value + "\n Rank: " + rank_editor.getSelectedItem().toString()  + " \n Name: " + name_editor.getText().toString() + "\n Gender: " + gender_value + " \n Squadron: " + squadron_editor.getText().toString() + " \n Duty Phone: " + phonenumber_editor.getText().toString() + "\n Base: " + base_value + "\n Building Number: " + building_editor.getText().toString() + "\n Room Number: " + room_editor.getText().toString() + " \n Repair Urgency: " + urgency_value + "\n Description: " + desc_editor.getText().toString() + " \n Date and Time: " + getCurrentTimeStamp() ;

        String mailto = "mailto:86ces.cehd.1@us.af.mil" +
                "?subject=" + Uri.encode(subject) +
                "&body=" + Uri.encode(body);


        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse(mailto));







        try {
            startActivity(emailIntent);
            finish();
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "no email app is available", Toast.LENGTH_SHORT);
            //TODO: Handle case where no email app is available
        }
    }

    private final Callback<Void> callCallback = new Callback<Void>() {
        @Override
        public void onResponse(Call<Void> call, Response<Void> response) {
            Log.d("XXX", "Submitted. " + response);
        }

        @Override
        public void onFailure(Call<Void> call, Throwable t) {
            Log.e("XXX", "Failed", t);
        }
    };










    void callEmeregencyADL() {

        if( g_airman == null) return;
        if( g_airman.fname == null) return;

        final BGAAlertController alertController = new BGAAlertController(this, getResources().getString(R.string.app_name), "Do you want to call the On-Call ADL?\n If this is a life threatening Emergency Call 112. ", BGAAlertController.AlertControllerStyle.Alert);

        alertController.addAction(new BGAAlertAction("On Call ADL", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01735863614"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }));
        alertController.addAction(new BGAAlertAction( "Call 112", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "112"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }));

     /*   final Activity activity = this;

        alertController.addAction(new BGAAlertAction( "Share Contact", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {

                String content = "Contact information: \n" + "01735863614" + "\nOn Call ADL";

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, content);
                try {
                    startActivity(Intent.createChooser(i, "Share via"));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(activity, "There is no sharing client installed.", Toast.LENGTH_SHORT);
                }
            }
        }));*/
        alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {
                alertController.dismiss();
            }
        }));

        alertController.setCancelable(true);
        alertController.show();
        ((RadioButton) findViewById(R.id.urgency_routine)).setChecked(true);


    }


}
