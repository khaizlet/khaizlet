package com.rabdorms.forcemobilestudios.activity;

import android.widget.Spinner;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface questionsheetservice {
    @POST("e/1FAIpQLSeK3ZxWaDh-fop_3QIr1_OSBuwpE3H34CY2XGQLpZetN0QqWw/formResponse")
    @FormUrlEncoded
    Call<Void> completeQuestionnaire(

            @Field("entry.282463114") String status,
            @Field("entry.870129834") String rank,
            @Field("entry.1262154330") String name,
            @Field("entry.1679596493") String squadron,
            @Field("entry.1329482112") String duty_phone,
            @Field("entry.1638020423") String base,
            @Field("entry.1215032533") String bldg_num,
            @Field("entry.1164018304") String room_num,
            @Field("entry.1069128389") String urgency,
          //  @Field("entry.1421236235") String typedesc,
            @Field("entry.822591483") String OS,
            @Field("entry.1768994780") String desc);



}



