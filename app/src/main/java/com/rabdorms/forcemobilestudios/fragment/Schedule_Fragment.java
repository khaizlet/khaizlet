package com.rabdorms.forcemobilestudios.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.rabdorms.forcemobilestudios.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Schedule_Fragment extends Fragment {


    private ProgressDialog dialog;
    ArrayList<String> listItems=new ArrayList<String>();
    ArrayAdapter<String> adapter;

Activity activity;
    String[] name;
    String[] rep;
    String[] email;
    String[] location;
    Context context;



    public Schedule_Fragment()  {

        // Required empty public constructor
//        fetchData process = new fetchData(this);
        //       process.execute();

    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String aboutus = " ";
        TextView ftac_Message;
        ListView listview;
       // activity = this;

        final View view = inflater.inflate(R.layout.schedule_layout, container, false);
      //  TextView text = (TextView) view.findViewById(R.id.ftac_Message);
        listview = (ListView) view.findViewById(R.id.listview);
        ProgressDialog ez = new ProgressDialog(getActivity());

        fetchData process = new fetchData(this);
        process.execute();

        return view;


    }


    public class fetchData extends AsyncTask<Void, Void, Void> {

        String data = "";
        String aboutus = "";
        String goal = "";

        String [] times;
        String [] activities;
        String [] instructor;
        String [] dayweek;
        String [] uniform;
        ListView listview;

        String[] times_temp;
        String[] activities_temp;
        String[] instructor_temp;
        String[] dayweek_temp;
        String [] uniform_temp;
        int num = 0;
        private ProgressDialog dialog;

        public fetchData(Schedule_Fragment ez) {
            dialog = new ProgressDialog(getActivity());
        }



        @Override
        protected void onPreExecute() {
            dialog.setMessage("Loading data, please wait...");
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL url = new URL("https://script.google.com/macros/s/AKfycbzlID3rUuwawkbf1LT3Bju1r0A-ADQsMKnqVjDJG8WC57u10bU/exec");
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                while (line != null) {
                    line = bufferedReader.readLine();
                    if (line != null)
                        data = data + line;
                }

                JSONObject jo_original = new JSONObject(data);
                JSONArray jsonArray = jo_original.getJSONArray("Test");
                JSONObject jo_each = (JSONObject) jsonArray.get(0);
//                aboutus = jo_each.getString("Message");


                times_temp = new String[jsonArray.length()];
                activities_temp = new String[jsonArray.length()];
                instructor_temp = new String[jsonArray.length()];
                dayweek_temp = new String[jsonArray.length()];
                uniform_temp = new String[jsonArray.length()];



                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject JO = (JSONObject) jsonArray.get(i);
                    //  if(!(JO.getString("email").equals("")||JO.getString("email")==null))
                    //
                    times_temp[num] = JO.getString("Times");

                    activities_temp[num] = JO.getString("Activities");
                    instructor_temp[num] = JO.getString("Instructor");
                    dayweek_temp[num] = JO.getString("DayWeek");
                    uniform_temp[num] = JO.getString("Uniform");
                    num++;

                }



            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (dialog.isShowing()) {
                dialog.dismiss();
            }


            times = new String[num];
            activities = new String[num];
            instructor = new String[num];
            dayweek = new String[num];
            uniform = new String[num];


            for(int i=0;i<num;i++){
               times[i] = times_temp[i];
                activities[i] = activities_temp[i];
                instructor[i] = instructor_temp[i];
                dayweek[i] = dayweek_temp[i];
                uniform[i] = uniform_temp[i];
            }

           // list_adapter = new FTAC_Schedule((Schedule_Fragment) ez,times, name, rep, location, email);

          //  String [] menu = {"jo","jim"};
//            listview.setAdapter(list_adapter);


     //      ArrayList<String[]> menu = new ArrayList<String[]>();
            ArrayList<String> menu= new ArrayList<String>();
           /* for (int i = 0; i < times.length; i++) {
                menu.add(times[0]+"\n"+activities);
                menu.add("\n");
                menu.add(activities[0]);
            }*/
           // _array.add("\n");
            menu.addAll(Arrays.asList(dayweek[0]+"\n"+ times[0] + "\n" + activities[0]+"\n"+instructor[0]+"\n"+uniform[0]+"\n"+"\n"+
                    dayweek[1]+"\n"+ times[1] + "\n" + activities[1]+"\n"+instructor[1]+"\n"+uniform[1]
            +"\n"+"\n"+dayweek[2]+"\n"+ times[2] + "\n" + activities[2]+"\n"+instructor[2]+"\n"+uniform[2]
           +"\n"+"\n"+ dayweek[3]+"\n"+ times[3] + "\n" + activities[3]+"\n"+instructor[3]+"\n"+uniform[3]
            +"\n"+"\n"+ dayweek[4]+"\n"+ times[4] + "\n" + activities[4]+"\n"+instructor[4]+"\n"+uniform[4]
            +"\n"+"\n"+dayweek[5]+"\n"+ times[5] + "\n" + activities[5]+"\n"+instructor[5]+"\n"+uniform[5]
            +"\n"+"\n"+dayweek[6]+"\n"+ times[6] + "\n" + activities[6]+"\n"+instructor[6]+"\n"+uniform[6]
            +"\n"+"\n"+dayweek[7]+"\n"+ times[7] + "\n" + activities[7]+"\n"+instructor[7]+"\n"+uniform[7]
            +"\n"+"\n"+dayweek[8]+"\n"+ times[8] + "\n" + activities[8]+"\n"+instructor[8]+"\n"+uniform[8]
            +"\n"+"\n"+dayweek[9]+"\n"+ times[9] + "\n" + activities[9]+"\n"+instructor[9]+"\n"+uniform[9]
            +"\n"+"\n"+dayweek[10]+"\n"+ times[10] + "\n" + activities[10]+"\n"+instructor[10]+"\n"+uniform[10]
            +"\n"+"\n"+dayweek[11]+"\n"+ times[11] + "\n" + activities[11]+"\n"+instructor[11]+"\n"+uniform[11]
            +"\n"+"\n"+dayweek[12]+"\n"+ times[12] + "\n" + activities[12]+"\n"+instructor[12]+"\n"+uniform[12]
            +"\n"+"\n"+dayweek[13]+"\n"+ times[13] + "\n" + activities[13]+"\n"+instructor[13]+"\n"+uniform[13]
            +"\n"+"\n"+dayweek[14]+"\n"+ times[14] + "\n" + activities[14]+"\n"+instructor[14]+"\n"+uniform[14]
            +"\n"+"\n"+dayweek[15]+"\n"+ times[15] + "\n" + activities[15]+"\n"+instructor[15]+"\n"+uniform[15]
            +"\n"+"\n"+dayweek[16]+"\n"+ times[16] + "\n" + activities[16]+"\n"+instructor[16]+"\n"+uniform[16]
                            +"\n"+"\n"+dayweek[17]+"\n"+ times[17] + "\n" + activities[17]+"\n"+instructor[17]+"\n"+uniform[17]
                            +"\n"+"\n"+dayweek[18]+"\n"+ times[18] + "\n" + activities[18]+"\n"+instructor[18]+"\n"+uniform[18]
                            +"\n"+"\n"+dayweek[19]+"\n"+ times[19] + "\n" + activities[19]+"\n"+instructor[19]+"\n"+uniform[19]
                            +"\n"+"\n"+dayweek[20]+"\n"+ times[20] + "\n" + activities[20]+"\n"+instructor[20]+"\n"+uniform[20]
                            +"\n"+"\n"+dayweek[21]+"\n"+ times[21] + "\n" + activities[21]+"\n"+instructor[21]+"\n"+uniform[21]
                            +"\n"+"\n"+dayweek[22]+"\n"+ times[22] + "\n" + activities[22]+"\n"+instructor[22]+"\n"+uniform[22]
                            +"\n"+"\n"+dayweek[23]+"\n"+ times[23] + "\n" + activities[23]+"\n"+instructor[23]+"\n"+uniform[23]
                            +"\n"+"\n"+dayweek[24]+"\n"+ times[24] + "\n" + activities[24]+"\n"+instructor[24]+"\n"+uniform[24]
                            +"\n"+"\n"+dayweek[25]+"\n"+ times[25] + "\n" + activities[25]+"\n"+instructor[25]+"\n"+uniform[25]
                            +"\n"+"\n"+dayweek[26]+"\n"+ times[26] + "\n" + activities[26]+"\n"+instructor[26]+"\n"+uniform[26]
                            +"\n"+"\n"+dayweek[27]+"\n"+ times[27] + "\n" + activities[27]+"\n"+instructor[27]+"\n"+uniform[27]
                            +"\n"+"\n"+dayweek[28]+"\n"+ times[28] + "\n" + activities[28]+"\n"+instructor[28]+"\n"+uniform[28]
                            +"\n"+"\n"+dayweek[29]+"\n"+ times[29] + "\n" + activities[29]+"\n"+instructor[29]+"\n"+uniform[29]
                            +"\n"+"\n"+dayweek[30]+"\n"+ times[30] + "\n" + activities[30]+"\n"+instructor[30]+"\n"+uniform[30]
                            +"\n"+"\n"+dayweek[31]+"\n"+ times[31] + "\n" + activities[31]+"\n"+instructor[31]+"\n"+uniform[31]
                            +"\n"+"\n"+dayweek[32]+"\n"+ times[32] + "\n" + activities[32]+"\n"+instructor[32]+"\n"+uniform[32]
                            +"\n"+"\n"+dayweek[33]+"\n"+ times[33] + "\n" + activities[33]+"\n"+instructor[33]+"\n"+uniform[33]
                            +"\n"+"\n"+dayweek[34]+"\n"+ times[34] + "\n" + activities[34]+"\n"+instructor[34]+"\n"+uniform[34]
                            +"\n"+"\n"+dayweek[35]+"\n"+ times[35] + "\n" + activities[35]+"\n"+instructor[35]+"\n"+uniform[35]
                            +"\n"+"\n"+dayweek[36]+"\n"+ times[36] + "\n" + activities[36]+"\n"+instructor[36]+"\n"+uniform[36]
                            +"\n"+"\n"+dayweek[37]+"\n"+ times[37] + "\n" + activities[37]+"\n"+instructor[37]+"\n"+uniform[37]
                            +"\n"+"\n"+dayweek[38]+"\n"+ times[38] + "\n" + activities[38]+"\n"+instructor[38]+"\n"+uniform[38]
                            +"\n"+"\n"+dayweek[39]+"\n"+ times[39] + "\n" + activities[39]+"\n"+instructor[39]+"\n"+uniform[39]
                            +"\n"+"\n"+dayweek[40]+"\n"+ times[40] + "\n" + activities[40]+"\n"+instructor[40]+"\n"+uniform[40]
                            +"\n"+"\n"+dayweek[41]+"\n"+ times[41] + "\n" + activities[41]+"\n"+instructor[41]+"\n"+uniform[41]
                            +"\n"+"\n"+dayweek[42]+"\n"+ times[42] + "\n" + activities[42]+"\n"+instructor[42]+"\n"+uniform[42]
                            +"\n"+"\n"+dayweek[43]+"\n"+ times[43] + "\n" + activities[43]+"\n"+instructor[43]+"\n"+uniform[43]
                            +"\n"+"\n"+dayweek[44]+"\n"+ times[44] + "\n" + activities[44]+"\n"+instructor[44]+"\n"+uniform[44]
                            +"\n"+"\n"+dayweek[45]+"\n"+ times[45] + "\n" + activities[45]+"\n"+instructor[45]+"\n"+uniform[45]
                            +"\n"+"\n"+dayweek[46]+"\n"+ times[46] + "\n" + activities[46]+"\n"+instructor[46]+"\n"+uniform[46]
                            +"\n"+"\n"+dayweek[47]+"\n"+ times[47] + "\n" + activities[47]+"\n"+instructor[47]+"\n"+uniform[47]
                            +"\n"+"\n"+dayweek[48]+"\n"+ times[48] + "\n" + activities[48]+"\n"+instructor[48]+"\n"+uniform[48]
                            +"\n"+"\n"+dayweek[49]+"\n"+ times[49] + "\n" + activities[49]+"\n"+instructor[49]+"\n"+uniform[49]
                            +"\n"+"\n"+dayweek[50]+"\n"+ times[50] + "\n" + activities[50]+"\n"+instructor[50]+"\n"+uniform[50]
                            +"\n"+"\n"+dayweek[51]+"\n"+ times[51] + "\n" + activities[51]+"\n"+instructor[51]+"\n"+uniform[51]
                            +"\n"+"\n"+dayweek[52]+"\n"+ times[52] + "\n" + activities[52]+"\n"+instructor[52]+"\n"+uniform[52]
                            +"\n"+"\n"+dayweek[53]+"\n"+ times[53] + "\n" + activities[53]+"\n"+instructor[53]+"\n"+uniform[53]
                            +"\n"+"\n"+dayweek[54]+"\n"+ times[54] + "\n" + activities[54]+"\n"+instructor[54]+"\n"+uniform[54]
                            +"\n"+"\n"+dayweek[55]+"\n"+ times[55] + "\n" + activities[55]+"\n"+instructor[55]+"\n"+uniform[55]


            ));


            //   List<String> exlist = new ArrayList<>(Arrays.asList(menu));
          // menu.add(times);
           ListView brands = (ListView) getView().findViewById(R.id.listview);

            ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(getActivity(),R.layout.schedule_layout,R.id.textView2,menu);

            brands.setAdapter(listViewAdapter);






        }
    }









}

