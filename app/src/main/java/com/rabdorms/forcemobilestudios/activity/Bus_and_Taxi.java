package com.rabdorms.forcemobilestudios.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import com.github.barteksc.pdfviewer.PDFView;
import com.rabdorms.forcemobilestudios.R;

import java.net.MalformedURLException;
import java.net.URL;

public class Bus_and_Taxi extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bus_and_taxi);
     PDFView pdfView1 = (PDFView) findViewById(R.id.pdfview);



        pdfView1.fromAsset("Bus_and_Taxi.pdf").load();

    }
}