package com.rabdorms.forcemobilestudios.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.alex.common.utils.AppUtil;
import com.rabdorms.forcemobilestudios.R;


import cn.bingoogolapple.alertcontroller.BGAAlertAction;
import cn.bingoogolapple.alertcontroller.BGAAlertController;
import info.hoang8f.android.segmented.SegmentedGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class GymFragment extends Fragment implements View.OnClickListener {
    long a = Long.parseLong("06315367866");
    String ss = String.valueOf(a);

    long b = Long.parseLong("06371470294");
    String ssfc = String.valueOf(b);

    long c = Long.parseLong("06315367329");
    String kgym = String.valueOf(c);

    Button south_gym_call_it;
    Button south_gym;
    Button north_gym_call_it;
    Button north_gym;
    Button kapaun_gym_it;
    Button kapaun_gym_call_it;

    public boolean isKapaun = false;
    public int checkedId;

    public GymFragment() {
        // Required empty public constructor
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(isKapaun ? R.layout.kapaun_gym : R.layout.ramstein_gym, container, false);

        if (isKapaun == false) {

            SegmentedGroup segment = (SegmentedGroup) view.findViewById(R.id.segment);

            segment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    LinearLayout south_gym_layout = (LinearLayout) view.findViewById(R.id.south_gym_layout);
                    LinearLayout north_gym_layout = (LinearLayout) view.findViewById(R.id.north_gym_layout);

                    if (checkedId == R.id.segment_south) {
                        south_gym_layout.setVisibility(View.VISIBLE);
                        north_gym_layout.setVisibility(View.GONE);


                    } else {
                        south_gym_layout.setVisibility(View.GONE);
                        north_gym_layout.setVisibility(View.VISIBLE);


                    }
                }
            });

        }



        if(isKapaun == false) {



            south_gym_call_it = (Button) view.findViewById(R.id.south_gym_call);
            south_gym = (Button) view.findViewById(R.id.south_gym);
           north_gym_call_it = (Button) view.findViewById(R.id.north_gym_call);
            north_gym = (Button) view.findViewById(R.id.north_gym_bldg);

            south_gym_call_it.setOnClickListener(this);
           south_gym.setOnClickListener(this);
            north_gym.setOnClickListener(this);
            north_gym_call_it.setOnClickListener(this);



        }






        if (isKapaun == true)

        {

            kapaun_gym_call_it = (Button) view.findViewById(R.id.kapaun_gym_call);
            kapaun_gym_it = (Button) view.findViewById(R.id.kapaun_gym);
            kapaun_gym_call_it.setOnClickListener(this);
            kapaun_gym_it.setOnClickListener(this);




        }


        // Inflate the layout for this fragment
        return view;
    }


    public void onClick(final View v) {


        if (isKapaun) {
            {

                if (v == kapaun_gym_call_it) {


                    final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "Do you want to call Vogelweh Gym?", BGAAlertController.AlertControllerStyle.Alert);

                    alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                        @Override
                        public void onClick() {
                        }
                    }));
                    alertController.addAction(new BGAAlertAction("Call", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                        @Override
                        public void onClick() {
                            AppUtil.callPhone(getActivity(), kgym);
                        }
                    }));
                    alertController.setCancelable(false);
                    alertController.show();


                }
            }


        }


        {
            if ((v == south_gym_call_it) ) {

                String msg = "Do you want to call the South Side Gym?";

                final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), msg, BGAAlertController.AlertControllerStyle.Alert);

                alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {


                    }
                }));
                alertController.addAction(new BGAAlertAction("Call", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {

                        AppUtil.callPhone(getActivity(), ssfc);


                    }
                }));
                alertController.setCancelable(false);
                alertController.show();

            }





                if ((v == north_gym_call_it) ) {

                    String msg = "Do you want to call the North Side Gym?";

                    final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), msg, BGAAlertController.AlertControllerStyle.Alert);

                    alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                        @Override
                        public void onClick() {


                        }
                    }));
                    alertController.addAction(new BGAAlertAction("Call", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                        @Override
                        public void onClick() {

                            AppUtil.callPhone(getActivity(), ssfc);


                        }
                    }));
                    alertController.setCancelable(false);
                    alertController.show();
                }



             else {

                // isKapaun = true;

                if (isKapaun) {

                    if (v == kapaun_gym_it) {

                        final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "Go to Vogelweh Gym?", BGAAlertController.AlertControllerStyle.Alert);
                        alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                            @Override
                            public void onClick() {
                            }
                        }));


                        alertController.addAction(new BGAAlertAction("Vogelweh Gym", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                            @Override
                            public void onClick() {

                                AppUtil.openGoogleMap(getActivity(), "49.435326", "7.700654");

                                // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                                String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.435326 + "," + 7.700654 + " (" + "Vogelweh Gym" + ")";
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                                //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                                startActivity(intent);


                            }
                        }));
                        alertController.setCancelable(false);
                        alertController.show();


                    }

                }


            }




            if (v == north_gym) {


                final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "North Side Gym", BGAAlertController.AlertControllerStyle.Alert);
                alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {
                    }
                }));


                alertController.addAction(new BGAAlertAction("Go to the North Side Gym?", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {

                        AppUtil.openGoogleMap(getActivity(), "49.454487", "7.599649");

                        // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                        String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.454487 + "," + 7.599649 + " (" + "North Side Gym" + ")";
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                        //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                        startActivity(intent);


                    }
                }));
                alertController.setCancelable(false);
                alertController.show();


            }


            if (v == south_gym) {


                final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "South Side Gym", BGAAlertController.AlertControllerStyle.Alert);
                alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {
                    }
                }));


                alertController.addAction(new BGAAlertAction("Go to the South Side Fitness Gym?", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {

                        AppUtil.openGoogleMap(getActivity(), "49.447319", "7.608245");

                        // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                        String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.447319 + "," + 7.608245 + " (" + "South Side Gym" + ")";
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                        //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                        startActivity(intent);


                    }
                }));
                alertController.setCancelable(false);
                alertController.show();


            }



        }


    }
}
//}
