package com.rabdorms.forcemobilestudios.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alex.common.utils.AppUtil;
import com.rabdorms.forcemobilestudios.R;
import com.rabdorms.forcemobilestudios.activity.ProfileEditActivity;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.synnapps.carouselview.ViewListener;


public class AlertOnBoardingDialog   implements View.OnClickListener  {

  //  initSplash(ConfigSplash configSplash);
    static Activity activity;

    AlertDialog dialog;

    CarouselView carouselView;
    int[] images = {R.mipmap.img_welcome01, R.mipmap.img_welcome02, R.mipmap.img_welcome03};
    String[] titles = {"WELCOME TO RAB DORMS", "SUBMIT YOUR WORK ORDER\n ON THE GO.","CREATE YOUR ACCOUNT"};
    String[] contents = {" ","", "Setting up your account takes only a few seconds."};

    public static void show(Activity activity) {
        AlertOnBoardingDialog.activity = activity;
        AlertOnBoardingDialog dialog = new AlertOnBoardingDialog();
    }
    public AlertOnBoardingDialog() {

       // mConfigSplash = new ConfigSplash();


        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.dialog_alert_onboarding, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setCancelable(false);
        dialog = alertDialogBuilder.create();
        dialog.setView(view);

        final Button signupButton = (Button) view.findViewById(R.id.signup_button);
        signupButton.setOnClickListener(this);

        carouselView = (CarouselView)view.findViewById(R.id.carousel_view);
        carouselView.setPageCount(images.length);
        carouselView.setViewListener(viewListener);
      //  carouselView.setImageListener(imageListener);



        carouselView.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {




            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if( position == 2) {
                    signupButton.setVisibility(View.VISIBLE);
                } else {
                    signupButton.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });













        if( !activity.isFinishing()) {

            dialog.show();
        }
    }









    ViewListener viewListener = new ViewListener() {
        @Override
        public View setViewForPosition(int position) {

            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View view = inflater.inflate(R.layout.view_welcome_page, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
            imageView.setImageResource(images[position]);

            TextView titleLabel = (TextView)view.findViewById(R.id.title_label);
            titleLabel.setText(titles[position]);

            TextView contentLabel = (TextView)view.findViewById(R.id.content_label);
            contentLabel.setText(contents[position]);

            return view;
        }
    };

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(images[position]);
        }
    };





    @Override
    public void onClick(View v) {

        dialog.dismiss();
        AppUtil.setSettingsString("true", "IS_APP_FIRST_LAUNCH");
        Intent intent = new Intent( activity, splash.class);
        activity.startActivity(intent);
    }
}
