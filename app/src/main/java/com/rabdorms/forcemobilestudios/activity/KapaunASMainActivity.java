package com.rabdorms.forcemobilestudios.activity;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.alex.common.adapter.ViewPagerAdapter;
import com.alex.common.helper.BottomNavigationViewHelper;
import com.rabdorms.forcemobilestudios.R;
import com.rabdorms.forcemobilestudios.fragment.DFACFragment;
import com.rabdorms.forcemobilestudios.fragment.DRCFragment;
import com.rabdorms.forcemobilestudios.fragment.GymFragment;
import com.rabdorms.forcemobilestudios.fragment.PostalFragment;
//import com.rabdorms.forcemobilestudios.fragment.GymFragment;

public class KapaunASMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kapaun_asmain);

        final BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);

        BottomNavigationViewHelper.removeShiftMode(navigation);

        final ViewPager viewPager = (ViewPager)findViewById(R.id.view_pager);
        viewPager.setOffscreenPageLimit(5);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        DRCFragment drcFragment = new DRCFragment(); drcFragment.isKapaun = true;
        DFACFragment dfacFragment = new DFACFragment(); dfacFragment.isKapaun = true;
        PostalFragment postalFragment = new PostalFragment(); postalFragment.isKapaun = true;
        GymFragment gymFragment = new GymFragment(); gymFragment.isKapaun = true;

        adapter.addFragment( drcFragment, "DAC");
        adapter.addFragment( dfacFragment, "DFAC Facilities");
        adapter.addFragment( postalFragment, "Postal Office");
        adapter.addFragment( gymFragment,"Kapaun Gym");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                int[] itemids = {R.id.navigation_drc, R.id.navigation_dfac, R.id.navigation_postal, R.id.cardiogram};

                navigation.setSelectedItemId(itemids[position]);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigation_drc:
                        viewPager.setCurrentItem(0);
                        return true;
                    case R.id.navigation_dfac:
                        viewPager.setCurrentItem(1);
                        return true;
                    case R.id.navigation_postal:
                        viewPager.setCurrentItem(2);
                        return true;
                    case R.id.cardiogram:
                        viewPager.setCurrentItem(3);
                        return true;
                }

                return true;
            }
        });

    }
}
