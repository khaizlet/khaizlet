package com.rabdorms.forcemobilestudios.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alex.common.utils.AppUtil;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.rabdorms.forcemobilestudios.R;
import com.rabdorms.forcemobilestudios.dialog.AlertOnBoardingDialog;
import com.rabdorms.forcemobilestudios.dialog.splash;
import com.rabdorms.forcemobilestudios.model.AirmanInfo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import cn.bingoogolapple.alertcontroller.BGAAlertAction;
import cn.bingoogolapple.alertcontroller.BGAAlertController;

import static com.rabdorms.forcemobilestudios.model.AirmanInfo.g_airman;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView navigationView;
    TextView greetingLabel;
    TextView dateLabel;
    TextView timeLabel;

    TextView airman_label;
    TextView building_label;
    TextView room_label;
    TextView location_label;

    TextView onCallADL_label;
    TextView onCallPhone_label;
    TextView hoursOfOperation_label;
    TextView addressDRC_label;





    Button callDRC_button;
    Button openMap_button;

    ImageView drcstatus_imageview;
    ImageView background_imageview;

    String[] rank_captions = {" ", "AB", "Amn", "A1C", "SrA", "SSgt", "TSgt", "MSgt", "SMSgt", "CMSgt"};
    String[] rank_captions2 = {" ", "SSgt", "TSgt", "MSgt", "SMSgt", "CMSgt"};
    String[] rank_captions3 = {" ", "MSgt", "SMSgt", "CMSgt"};





    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        String json_string = AppUtil.getSettingsString("g_airman");
        if (json_string != null) {
            Gson gson = new Gson();
            g_airman = gson.fromJson(json_string, AirmanInfo.class);
        }

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FirebaseMessaging.getInstance().subscribeToTopic("News");


        initialize();









    }

















    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if ((g_airman == null) || (g_airman.fname == null)) {

          //AlertOnBoardingDialog.show(this);
            Intent intent = new Intent( this, splash.class);
            this.startActivity(intent);

           //splash.show(this);

             // <----- this

        }
        //}

        updateView();
    }

    static Handler nav_handler = new Handler();

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle ramstein_tabmenu view item clicks here.

        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_dorms_guide) {
            Intent intent = new Intent(this, DormsGuideActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_repair_request) {
            Intent intent = new Intent(this, RepairRequestActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_room_lockout) {
            Intent intent = new Intent(this, RoomLockoutActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_ask_adl) {
            Intent intent = new Intent(this, AskADLActivity.class);
            startActivity(intent);
        } else if (id == R.id.FTAC) {
            Intent intent = new Intent(this, ftac_Activity.class);
            startActivity(intent);
        } else if (id == R.id.nav_ramstein) {
            Intent intent = new Intent(this, RamsteinMainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_kapaun_air_station) {
            Intent intent = new Intent(this, KapaunASMainActivity.class);
            startActivity(intent);
        } else if (id == R.id.Bus_and_Taxi) {
            Intent intent = new Intent(this, Bus_and_Taxi.class);
            startActivity(intent);
        } else if (id == R.id.nav_emergency_numbers) {
            Intent intent = new Intent(this, EmergencyPhonesActivity.class);
            startActivity(intent);
        }else if (id == R.id.share_a_ride) {
                Intent intent = new Intent(this, shareARide.class);
                startActivity(intent);
        } else if (id == R.id.nav_profile) {
            Intent intent = new Intent(this, ProfileEditActivity.class);
            startActivity(intent);
        } else if (id == R.id.support_squadron) {

            Intent intent = new Intent(this, Support_Squadron.class);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(this, Activity_Settings.class);
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);


        nav_handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigationView.setCheckedItem(R.id.nav_none);
            }
        }, 100);

        return true;
    }

    void initialize() {



        if(g_airman.lname == null){

            setContentView(R.layout.activity_main);

        }else

        greetingLabel = (TextView) findViewById(R.id.greeting_label);
        dateLabel = (TextView) findViewById(R.id.date_label);
        timeLabel = (TextView) findViewById(R.id.time_label);

        airman_label = (TextView) findViewById(R.id.airman_label);
        building_label = (TextView) findViewById(R.id.dorm_location_building_label);
        room_label = (TextView) findViewById(R.id.dorm_location_room_label);
        location_label = (TextView) findViewById(R.id.dorm_location_label);

        onCallADL_label = (TextView) findViewById(R.id.on_call_adl_label);
        onCallPhone_label = (TextView) findViewById(R.id.phonenumber_label);

        hoursOfOperation_label = (TextView) findViewById(R.id.hoursofoperation_label);
        addressDRC_label = (TextView) findViewById(R.id.address_label);

        callDRC_button = (Button) findViewById(R.id.call_drc_button);
        openMap_button = (Button) findViewById(R.id.open_map_button);

        drcstatus_imageview = (ImageView) findViewById(R.id.drc_status_imageview);
        background_imageview = (ImageView) findViewById(R.id.background_imageview);

        callDRC_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callDRC();
            }
        });
        openMap_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMaps();
            }
        });

        onCallPhone_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callEmeregencyADL();
            }
        });

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        timerHandler();
//                        Date currentTime = Calendar.getInstance().getTime();
//                        timeOfDayLabel.setText(currentTime.toString());
                    }
                });
            }
        }, 1000, 1000);

    }


    //ArrayAdapter<Spinner> getranks = new <Spinner> ArrayAdapter(onClick view);

    void updateView() {


        if (g_airman == null) return;
        if (g_airman.fname == null) return;

        if (g_airman.rank == 0) {

            airman_label.setText(rank_captions[g_airman.rankedit] + " " + g_airman.fname + " " + g_airman.lname);
            building_label.setText("Building: " + g_airman.building);
            room_label.setText("Room: " + g_airman.room);


        } else if (g_airman.rank == 1) {


            airman_label.setText(rank_captions[g_airman.rankedit] + " " + g_airman.fname + " " + g_airman.lname);
            building_label.setText("Supervisor");
            room_label.setText(" " + g_airman.squadron);

        } else if (g_airman.rank == 2) {

            airman_label.setText(rank_captions[g_airman.rankedit] + " " + g_airman.fname + " " + g_airman.lname);
            building_label.setText("First Sergeant");
            room_label.setText(" " + g_airman.squadron);

        }


        //   building_label.setText("Building: " + g_airman.building);
        //   room_label.setText("Room: " + g_airman.room);

        Date currentTime = Calendar.getInstance().getTime();

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentTime);

        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 7, 30, 0);
        Date seven_half_today = cal.getTime();

        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 16, 30, 0);
        Date four_half_today = cal.getTime();

        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 9, 0, 0);
        Date nine_monday = cal.getTime();

        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 16, 30, 0);
        Date four_half_monday = cal.getTime();

        String weekday = weekDateFormat.format(currentTime);

        if (g_airman.base == 0) { //
            location_label.setText("Ramstein Air Base");
            background_imageview.setImageResource(R.mipmap.img_back01);

            if (weekday.equals("Saturday") || weekday.equals("Sunday")) {

                onCallADL_label.setVisibility(View.VISIBLE);
                onCallPhone_label.setVisibility(View.VISIBLE);
                hoursOfOperation_label.setVisibility(View.GONE);
                addressDRC_label.setVisibility(View.GONE);
                callDRC_button.setVisibility(View.GONE);
                openMap_button.setVisibility(View.GONE);
                drcstatus_imageview.setImageResource(R.drawable.closed_2);

                onCallADL_label.setText("An ADL is On-Call \n for emergency purpose.");
                onCallPhone_label.setText("01735863614");
            } else {
                if (weekday.equals("Monday")) {
                    if (currentTime.after(nine_monday) && four_half_monday.after(currentTime)) {
                        onCallADL_label.setVisibility(View.GONE);
                        onCallPhone_label.setVisibility(View.GONE);
                        hoursOfOperation_label.setVisibility(View.VISIBLE);
                        addressDRC_label.setVisibility(View.VISIBLE);
                        callDRC_button.setVisibility(View.VISIBLE);
                        openMap_button.setVisibility(View.VISIBLE);
                        drcstatus_imageview.setImageResource(R.drawable.open_2);

                        hoursOfOperation_label.setText("Monday\n 0900 - 1630 \n Tuesday to Friday\n 0730 - 1630\n");
                        addressDRC_label.setText("Bldg 2108 \n(by the Finance Building)");
                    } else {
                        onCallADL_label.setVisibility(View.VISIBLE);
                        onCallPhone_label.setVisibility(View.VISIBLE);
                        hoursOfOperation_label.setVisibility(View.GONE);
                        addressDRC_label.setVisibility(View.GONE);
                        callDRC_button.setVisibility(View.GONE);
                        openMap_button.setVisibility(View.GONE);
                        drcstatus_imageview.setImageResource(R.drawable.closed_2);

                        onCallADL_label.setText("An ADL is On-Call \n for emergency purpose.");
                        onCallPhone_label.setText("01735863614");
                    }
                } else {
                    if (currentTime.after(seven_half_today) && four_half_today.after(currentTime)) {
                        onCallADL_label.setVisibility(View.GONE);
                        onCallPhone_label.setVisibility(View.GONE);
                        hoursOfOperation_label.setVisibility(View.VISIBLE);
                        addressDRC_label.setVisibility(View.VISIBLE);
                        callDRC_button.setVisibility(View.VISIBLE);
                        openMap_button.setVisibility(View.VISIBLE);
                        drcstatus_imageview.setImageResource(R.drawable.open_2);

                        hoursOfOperation_label.setText("Monday\n 0900 - 1630 \n Tuesday to Friday\n 0730 - 1630\n");
                        addressDRC_label.setText("Bldg 2108 \n(by the Finance Building)");
                    } else {
                        onCallADL_label.setVisibility(View.VISIBLE);
                        onCallPhone_label.setVisibility(View.VISIBLE);
                        hoursOfOperation_label.setVisibility(View.GONE);
                        addressDRC_label.setVisibility(View.GONE);
                        callDRC_button.setVisibility(View.GONE);
                        openMap_button.setVisibility(View.GONE);
                        drcstatus_imageview.setImageResource(R.drawable.closed_2);

                        onCallADL_label.setText("An ADL is On-Call \n for emergency purpose.");
                        onCallPhone_label.setText("01735863614");
                    }
                }
            }

        } else {
            location_label.setText("Kapaun Air Station");
            background_imageview.setImageResource(R.mipmap.img_back02);

            if (weekday.equals("Saturday") || weekday.equals("Sunday")) {
                onCallADL_label.setVisibility(View.VISIBLE);
                onCallPhone_label.setVisibility(View.VISIBLE);
                hoursOfOperation_label.setVisibility(View.GONE);
                addressDRC_label.setVisibility(View.GONE);
                callDRC_button.setVisibility(View.GONE);
                openMap_button.setVisibility(View.GONE);
                drcstatus_imageview.setImageResource(R.drawable.closed_2);

                onCallADL_label.setText("An ADL is On-Call \n for emergency purpose.");
                onCallPhone_label.setText("01735863614");
            } else {
                if (weekday.equals("Monday")) {
                    if (currentTime.after(nine_monday) && four_half_monday.after(currentTime)) {
                        onCallADL_label.setVisibility(View.GONE);
                        onCallPhone_label.setVisibility(View.GONE);
                        hoursOfOperation_label.setVisibility(View.VISIBLE);
                        addressDRC_label.setVisibility(View.VISIBLE);
                        callDRC_button.setVisibility(View.VISIBLE);
                        openMap_button.setVisibility(View.VISIBLE);
                        drcstatus_imageview.setImageResource(R.drawable.open_2);

                        hoursOfOperation_label.setText("Monday\n 0900 - 1630 \n Tuesday to Friday\n 0730 - 1630\n");
                        addressDRC_label.setText("Bldg 2771 \n(1st Floor))");
                    } else {
                        onCallADL_label.setVisibility(View.VISIBLE);
                        onCallPhone_label.setVisibility(View.VISIBLE);
                        hoursOfOperation_label.setVisibility(View.GONE);
                        addressDRC_label.setVisibility(View.GONE);
                        callDRC_button.setVisibility(View.GONE);
                        openMap_button.setVisibility(View.GONE);
                        drcstatus_imageview.setImageResource(R.drawable.closed_2);

                        onCallADL_label.setText("An ADL is On-Call \n for emergency purpose.");
                        onCallPhone_label.setText("01735863614");
                    }
                } else {
                    if (currentTime.after(seven_half_today) && four_half_today.after(currentTime)) {
                        onCallADL_label.setVisibility(View.GONE);
                        onCallPhone_label.setVisibility(View.GONE);
                        hoursOfOperation_label.setVisibility(View.VISIBLE);
                        addressDRC_label.setVisibility(View.VISIBLE);
                        callDRC_button.setVisibility(View.VISIBLE);
                        openMap_button.setVisibility(View.VISIBLE);
                        drcstatus_imageview.setImageResource(R.drawable.open_2);

                        hoursOfOperation_label.setText("Monday\n 0900 - 1630 \n Tuesday to Friday\n 0730 - 1630\n");
                        addressDRC_label.setText("Bldg 2771 \n(1st Floor)");
                    } else {
                        onCallADL_label.setVisibility(View.VISIBLE);
                        onCallPhone_label.setVisibility(View.VISIBLE);
                        hoursOfOperation_label.setVisibility(View.GONE);
                        addressDRC_label.setVisibility(View.GONE);
                        callDRC_button.setVisibility(View.GONE);
                        openMap_button.setVisibility(View.GONE);
                        drcstatus_imageview.setImageResource(R.drawable.closed_2);

                        onCallADL_label.setText("An ADL is On-Call \n for emergency purpose.");
                        onCallPhone_label.setText("01735863614");
                    }
                }
            }
        }
    }


    public static SimpleDateFormat weekDateFormat = new SimpleDateFormat("EEEE");
    public static SimpleDateFormat timeDateFormat = new SimpleDateFormat("HH:mm:ss");
    public static SimpleDateFormat dateDateFormat = new SimpleDateFormat("EEEE, MMMM d, yyyy");

    void timerHandler() {

        greetingLabel.setText(AppUtil.greeting());

        Date currentTime = Calendar.getInstance().getTime();

        timeLabel.setText("Local Time: " + timeDateFormat.format(currentTime));
        dateLabel.setText("Today is " + dateDateFormat.format(currentTime));


//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                Date currentTime = Calendar.getInstance().getTime();
//                timeOfDayLabel.setText(currentTime.toString());
//            }
//        });
    }

    void callDRC() {

        if (g_airman == null) return;
        if (g_airman.fname == null) return;

        final String dsn_phone, drc_phone;

        if (g_airman.base == 0) {
            dsn_phone = "480-3676";
            drc_phone = "49-6371-47-1xxx";
        } else {
            dsn_phone = "489-6876";
            drc_phone = "49-6371-47-1xxx";
        }

        final BGAAlertController alertController = new BGAAlertController(this, getResources().getString(R.string.app_name), "Do you want to call the Ramstein DRC?", BGAAlertController.AlertControllerStyle.Alert);

        alertController.addAction(new BGAAlertAction("DSN " + dsn_phone, BGAAlertAction.AlertActionStyle.Default, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {
                //showToast("DSN " + dsn_phone);
            }
        }));
        alertController.addAction(new BGAAlertAction("Call the DRC", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + drc_phone));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }));

        final Activity activity = this;

        alertController.addAction(new BGAAlertAction("Share Contact", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {

                String content = "Contact information: \n Comm " + drc_phone + "\n DSN " + dsn_phone + "\n Ramstein Air Base DRC";

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, content);
                try {
                    startActivity(Intent.createChooser(i, "Share via"));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(activity, "There is no sharing client installed.", Toast.LENGTH_SHORT);
                }
            }
        }));
        alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {
                //alertController.dismiss();
            }
        }));

        alertController.setCancelable(true);
        alertController.show();
    }

    void callEmeregencyADL() {

        if (g_airman == null) return;
        if (g_airman.fname == null) return;

        final BGAAlertController alertController = new BGAAlertController(this, getResources().getString(R.string.app_name), "Do you want to call the On-Call ADL?\n If this is a life threatening Emergency Call 112. ", BGAAlertController.AlertControllerStyle.Alert);

        alertController.addAction(new BGAAlertAction("On Call ADL", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "01735863614"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }));
        alertController.addAction(new BGAAlertAction("Call 112", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "112"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }));

        final Activity activity = this;

        alertController.addAction(new BGAAlertAction("Share Contact", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {

                String content = "Contact information: \n" + "01735863614" + "\nOn Call ADL";

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, content);
                try {
                    startActivity(Intent.createChooser(i, "Share via"));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(activity, "There is no sharing client installed.", Toast.LENGTH_SHORT);
                }
            }
        }));
        alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {
                //alertController.dismiss();
            }
        }));

        alertController.setCancelable(true);
        alertController.show();

    }

    void openMaps() {

        String msg = "Would you like to visit the DRC?";
        //final String lat = "49.445551";
        // final String lon = "7.605060";
        // final String mark_title = "DRC";
        final Activity activity = this;

        final BGAAlertController alertController = new BGAAlertController(this, getResources().getString(R.string.app_name), msg, BGAAlertController.AlertControllerStyle.Alert);

        alertController.addAction(new BGAAlertAction("DRC", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {


                AppUtil.openGoogleMap(activity, "49.445551", "7.605060");
                // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.445551 + "," + 7.605060 + " (" + "DRC" + ")";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                startActivity(intent);


            }
        }));
        alertController.setCancelable(false);
        alertController.show();


    }




    }

