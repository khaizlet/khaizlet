package com.alex.common.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;



public class UIUtil {

    static public Context applicationContext;

    static private ProgressDialog __hub_dialog;


    public interface QueryInterface {
        public void onSelected(int index);
    }

    static public Activity topActivity() {
        if( applicationContext == null) return null;

        ActivityManager am = (ActivityManager)applicationContext.getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;

        return null;
    }

    static public Activity getActivity(View view) {

        if(view == null) return null;

        Context context = view.getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

    static public int screenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    static public int screenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    static public void showMessage(Context context, String message, String button) {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(context);
        dlgAlert.setMessage(message);
        dlgAlert.setTitle("App Title");
        dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dlgAlert.setCancelable(false);
        dlgAlert.create().show();
    }

    static public void query(Context context, String message, String button_ok, String button_cancel, DialogInterface.OnClickListener ok_listener, DialogInterface.OnClickListener cancel_listener) {

        new android.support.v7.app.AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(button_ok, ok_listener)
                .setNegativeButton(button_cancel, cancel_listener)
                .create()
                .show();

    }

    static public void showHub() {
        UIUtil.showHub(UIUtil.topActivity());
    }

    static public void showHub( Context context) {

        if( context == null) return;


        if(__hub_dialog == null) {
            __hub_dialog = new ProgressDialog(context);
            __hub_dialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        }
        if(__hub_dialog.getContext() != context) {
            __hub_dialog = new ProgressDialog(context);
            __hub_dialog.setCancelable(false); // disable dismiss by tapping outside of the dialog
        }
        __hub_dialog.setTitle("Loading");
        __hub_dialog.setMessage("Wait while loading...");
        __hub_dialog.show();
    }

    static public void hideHub() {
        if(__hub_dialog != null) {
            __hub_dialog.dismiss();
        }
    }

    static public void setEnableAllViews( boolean isEnable, View view) {

        if( view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup)view).getChildCount(); i++){
                View child = ((ViewGroup)view).getChildAt(i);
                child.setEnabled(isEnable);
                if (child instanceof ViewGroup){
                    UIUtil.setEnableAllViews(isEnable, (ViewGroup)child);
                }
            }
        } else {
            view.setEnabled(isEnable);
        }
    }
}
