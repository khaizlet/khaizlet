package com.rabdorms.forcemobilestudios.activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.alex.common.utils.AppUtil;
import com.google.gson.Gson;
import com.rabdorms.forcemobilestudios.R;
import com.rabdorms.forcemobilestudios.model.AirmanInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cn.bingoogolapple.alertcontroller.BGAAlertAction;
import cn.bingoogolapple.alertcontroller.BGAAlertController;
import info.hoang8f.android.segmented.SegmentedGroup;

import static com.rabdorms.forcemobilestudios.R.id.Resident;
import static com.rabdorms.forcemobilestudios.R.id.Sgt;
import static com.rabdorms.forcemobilestudios.R.id.Supervisor;
import static com.rabdorms.forcemobilestudios.R.id.action_bar_spinner;
import static com.rabdorms.forcemobilestudios.R.id.dorm_location_building_label;
import static com.rabdorms.forcemobilestudios.R.id.parent;
import static com.rabdorms.forcemobilestudios.R.id.rank_editor;
import static com.rabdorms.forcemobilestudios.R.id.select_dialog_listview;
import static com.rabdorms.forcemobilestudios.model.AirmanInfo.g_airman;
import com.rabdorms.forcemobilestudios.activity.MainActivity;
import com.shashank.sony.fancytoastlib.FancyToast;

import org.w3c.dom.Text;

public class ProfileEditActivity extends AppCompatActivity implements View.OnClickListener {



    String sUsername;
    String sUserroom;
    String sUsersq;
    String sUserbuilding;
    String sUserphone;
    TextView name_editor;



   TextView lName ;


    TextView cellNum;


    TextView squadron_editor;
    TextView phonenumber_editor;
    TextView building_editor;
    TextView room_editor;
    TextView newranks;
    Spinner rank_editor;
    TextView building_label;
    int[] rank_editor2;



    //String myStringArray[] = {"2765", "2766", "2771", "2772", "2773", "2818", "2102", "2103", "2104", "2112", "2119", "2414", "2416", "2418", "2411", "2486", "2111", "N/A", null};

    SegmentedGroup rank_segment;
    SegmentedGroup gender_segment;
    SegmentedGroup base_segment;
    int[] rank_ids = {R.id.Resident, R.id.Supervisor, R.id.Sgt};
    int[] gender_ids = {R.id.gender_option_male, R.id.gender_option_female};
    int[] base_ids = {R.id.base_option_ramstein, R.id.base_option_kapaun};
    private EditText mEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (g_airman.fname == null) {
            setTitle("Create Profile");
        } else {
            setTitle("Profile");
        }


        setContentView(R.layout.activity_profile_sgt);


        initialize();















        /* name_editor.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }
            @Override
            public void afterTextChanged(Editable et) {
                String s=et.toString();
                if((!s.equals(s.toUpperCase())&& s.length()==1)) {
                    s=s.toUpperCase();
                    name_editor.setText(s);
                }
            }
        });*/


    }


























    @Override
    public void onBackPressed() {

        if (getTitle().equals("Profile")) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (getTitle().equals("Profile")) {
            getMenuInflater().inflate(R.menu.menu_profile2, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_profile, menu);
        }

        return true;//super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        name_editor = (TextView) findViewById(R.id.name_editor);
        lName = (TextView) findViewById(R.id.last_name);
        cellNum = (TextView) findViewById(R.id.cell_phone);

        squadron_editor = (TextView) findViewById(R.id.squadron_editor);
        room_editor = (TextView) findViewById(R.id.roomnumber_editor);
        phonenumber_editor = (TextView) findViewById(R.id.phone_editor);
        building_editor = (TextView) findViewById(R.id.buildingnumber_editor);
      //  rank_editor = (Spinner) findViewById(R.id.rank_editor) ;






        boolean arraycontains = false;


        int id = item.getItemId();



        //  String newAccount;



        if (id == R.id.profile_save) {
            String myStringArray[] = {"2765", "2766", "2771", "2772", "2773", "2818", "2102", "2103", "2104", "2112", "2119", "2414", "2416", "2418", "2411", "2486", "2111", "N/A"};

            boolean found = false;
            for (int d = 0; d < myStringArray.length; d++) {
                if (myStringArray[d].equals(building_editor.getText().toString())) {
                    found = true;
                }


            }


            if (name_editor.getText().toString().length() == 0) {
                FancyToast.makeText(getApplicationContext(), "Please enter your first name.", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();

           } else if (lName.getText().toString().length() == 0) {

                FancyToast.makeText(getApplicationContext(), "Please enter your last name.", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();

            } else if (cellNum.getText().toString().length() == 0) {

                FancyToast.makeText(getApplicationContext(), "Please enter your cell number.", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();



            } else if (squadron_editor.getText().toString().length() == 0) {

                FancyToast.makeText(getApplicationContext(), "Please enter your squadron.", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();

            } else if (phonenumber_editor.getText().toString().length() == 0) {

                FancyToast.makeText(getApplicationContext(), "Please enter a phone number.", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();

            } else if ((!found || building_editor.getText().toString().equals(null) ))

            {

                FancyToast.makeText(getApplicationContext(), "Please enter the correct building number.", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();



            } else if (room_editor.getText().toString().length() == 0 || room_editor.getText().toString().equals(" ")) {

                FancyToast.makeText(getApplicationContext(), "Please enter a room number.", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();

            } else if (rank_editor.getSelectedItem().equals("")) {

                FancyToast.makeText(getApplicationContext(), "Please select a rank.", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();



            } else


            {

                saveProfile();
                finish();
            }

        }


        if (id == R.id.profile_remove)

        {

            removeProfile();
            initialize();

        }
        return true;
    }


    void initialize() {

        Button f3 = (Button) findViewById(R.id.Resident);
        f3.setOnClickListener(this);

        Button f1 = (Button) findViewById(R.id.Sgt);
        f1.setOnClickListener(this);

        Button f2 = (Button) findViewById(R.id.Supervisor);
        f2.setOnClickListener(this);


 //       final Button view_reports = (Button)findViewById(R.id.report);


//        view_reports.setOnClickListener( this);


        TextView greetingLabel = (TextView) findViewById(R.id.greeting_label);
        greetingLabel.setText(AppUtil.greeting());


        rank_editor = (Spinner) findViewById(R.id.rank_editor) ;
        name_editor = (TextView) findViewById(R.id.name_editor);
        lName = (TextView) findViewById(R.id.last_name);
        cellNum = (TextView) findViewById(R.id.cell_phone);





        squadron_editor = (TextView) findViewById(R.id.squadron_editor);
        phonenumber_editor = (TextView) findViewById(R.id.phone_editor);
        building_editor = (TextView) findViewById(R.id.buildingnumber_editor);
        room_editor = (TextView) findViewById(R.id.roomnumber_editor);

        rank_segment = (SegmentedGroup) findViewById(R.id.rank_segment);
        gender_segment = (SegmentedGroup) findViewById(R.id.gender_segment);
        base_segment = (SegmentedGroup) findViewById(R.id.base_segment);

        ((RadioButton) findViewById(R.id.Resident)).setChecked(true);
        ((RadioButton) findViewById(R.id.Supervisor)).setChecked(false);
        ((RadioButton) findViewById(R.id.Sgt)).setChecked(false);

        ((RadioButton) findViewById(R.id.gender_option_male)).setChecked(true);
        ((RadioButton) findViewById(R.id.base_option_ramstein)).setChecked(true);




        Spinner spinner = (Spinner) findViewById(R.id.rank_editor);
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(ProfileEditActivity.this, R.layout.spinner_style,
                getResources().getStringArray(R.array.Ranks));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(myAdapter);



       /* Spinner spinner2 = (Spinner) findViewById(R.id.rank_editor);
        ArrayAdapter<String> myAdapter2 = new ArrayAdapter<String>(ProfileEditActivity.this, R.layout.spinner_style,
                getResources().getStringArray(R.array.Ranks));
        myAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(myAdapter2);


        Spinner spinner3 = (Spinner) findViewById(R.id.rank_editor);
        ArrayAdapter<String> myAdapter3 = new ArrayAdapter<String>(ProfileEditActivity.this, R.layout.spinner_style,
                getResources().getStringArray(R.array.Ranks));
        myAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner3.setAdapter(myAdapter3);*/


        if (g_airman.fname != null && g_airman.lname != null) {


            /* rank_editor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(ProfileEditActivity.this, rank_editor.getItemAtPosition(position).toString() + " found it ", Toast.LENGTH_LONG).show();
                    Object test = rank_editor.getSelectedItem();



                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });*/



            spinner.setSelection(g_airman.rankedit);


            rank_editor.setSelection(g_airman.rankedit);
            name_editor.setText(g_airman.fname);
            lName.setText(g_airman.lname);
            cellNum.setText(g_airman.cell);
            building_editor.setText(g_airman.building);
            phonenumber_editor.setText(g_airman.dutyPhone);
            room_editor.setText(g_airman.room);
            squadron_editor.setText(g_airman.squadron);






          RadioButton radioButton;

            radioButton = (RadioButton) findViewById(rank_ids[g_airman.rank]);
            radioButton.setChecked(true);

            radioButton = (RadioButton) findViewById(gender_ids[g_airman.gender]);
            radioButton.setChecked(true);

            radioButton = (RadioButton) findViewById(base_ids[g_airman.base]);
            radioButton.setChecked(true);


        }


        saveProfile();

    }


    void saveProfile() {




        g_airman.fname = name_editor.getText().toString();
        g_airman.lname = lName.getText().toString();
        g_airman.cell = cellNum.getText().toString();
        g_airman.building = building_editor.getText().toString();
        g_airman.dutyPhone = phonenumber_editor.getText().toString();
        g_airman.room = room_editor.getText().toString();
        g_airman.squadron = squadron_editor.getText().toString();


        g_airman.rank = AppUtil.getIndexOf(rank_ids, rank_segment.getCheckedRadioButtonId());
        g_airman.gender = AppUtil.getIndexOf(gender_ids, gender_segment.getCheckedRadioButtonId());
        g_airman.base = AppUtil.getIndexOf(base_ids, base_segment.getCheckedRadioButtonId());
        g_airman.rankedit = rank_editor.getSelectedItemPosition();


        Gson gson = new Gson();
        String json_string = gson.toJson(g_airman);
        AppUtil.setSettingsString(json_string, "g_airman");
    }

    void removeProfile() {

        final BGAAlertController alertController = new BGAAlertController(this, getResources().getString(R.string.app_name), "Would you like to DELETE your profile?", BGAAlertController.AlertControllerStyle.Alert);

        alertController.addAction(new BGAAlertAction("DELETE", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {
                g_airman = new AirmanInfo();
                AppUtil.removeSettingsString("g_airman");
                alertController.dismiss();
                finish();

            }
        }));
        alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {
            }

        }));

        alertController.setCancelable(true);
        alertController.show();

        new ProfileEditActivity();
    }











    @Override
    public void onClick(View v)

    {
       // if (rank_segment.getCheckedRadioButtonId() == R.id.Sgt)

        int i = v.getId();











        if (i == Resident) {




            Spinner spinner = (Spinner) findViewById(R.id.rank_editor);
            ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(ProfileEditActivity.this, R.layout.spinner_style,
                    getResources().getStringArray(R.array.Ranks));
            myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(myAdapter);


            building_editor.setClickable(true);
            building_editor.setText(null);
            building_editor.setEnabled(true);
            room_editor.setText(null);
            room_editor.setClickable(true);
            room_editor.setEnabled(true);





          //  g_airman.rankeditor = spinner;
          //  g_airman.rankeditor = spinner.getResources().getStringArray(R.array.Sgtranks).toString();


        } else if (i == Supervisor) {

            Spinner spinner = (Spinner) findViewById(R.id.rank_editor);
            ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(ProfileEditActivity.this, R.layout.spinner_style,
                    getResources().getStringArray(R.array.Ranks));
            myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(myAdapter);


            building_editor.setClickable(false);
            building_editor.setEnabled(false);
            building_editor.setText("N/A");
            room_editor.setClickable(false);
            room_editor.setEnabled(false);
            room_editor.setText("N/A");



        }else if(i == Sgt){



            Spinner spinner = (Spinner) findViewById(R.id.rank_editor);
            ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(ProfileEditActivity.this, R.layout.spinner_style,
                    getResources().getStringArray(R.array.Ranks));
            myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(myAdapter);

            building_editor.setClickable(false);
            building_editor.setEnabled(false);
            building_editor.setText("N/A");
            room_editor.setClickable(false);
            room_editor.setEnabled(false);
            room_editor.setText("N/A");
        }





        }










    }














