package com.rabdorms.forcemobilestudios.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.alex.common.adapter.ViewPagerAdapter;
import com.alex.common.helper.BottomNavigationViewHelper;
import com.rabdorms.forcemobilestudios.R;
import com.rabdorms.forcemobilestudios.fragment.Force_Support_it;
import com.rabdorms.forcemobilestudios.fragment.SARide;
import com.rabdorms.forcemobilestudios.fragment.Support_Calendar;


public class shareARide extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sharearide);

       // final BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);

       // BottomNavigationViewHelper.removeShiftMode(navigation);

        final ViewPager viewPager = (ViewPager)findViewById(R.id.view_pager);
        viewPager.setOffscreenPageLimit(2);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new SARide(), "SAR1");
        adapter.addFragment(new SARide(), "SAR2");
        viewPager.setAdapter(adapter);

       /*  viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

           @Override
            public void onPageSelected(int position) {

                int[] itemids = {R.id.Support_Event, R.id.Force_Support_it};

                navigation.setSelectedItemId(itemids[position]);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.Support_Event:
                        viewPager.setCurrentItem(0);
                        return true;
                    case R.id.Force_Support_it:
                        viewPager.setCurrentItem(1);

                        return true;


                }


                return true;
            }
        });*/
    }





}
