package com.rabdorms.forcemobilestudios.activity;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rabdorms.forcemobilestudios.R;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    static public double lat;
    static public double lon;
    static public String mark_title;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        float zoomLevel = 18;



        LatLng kiosk = new LatLng(lat, lon);
        mMap.addMarker(new MarkerOptions().position(kiosk).title(mark_title));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(kiosk));
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(kiosk, zoomLevel));



    }
}
