package com.rabdorms.forcemobilestudios.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.rabdorms.forcemobilestudios.R;
import com.rabdorms.forcemobilestudios.cell.EmergencyPhoneCell;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cn.bingoogolapple.alertcontroller.BGAAlertAction;
import cn.bingoogolapple.alertcontroller.BGAAlertController;

public class EmergencyPhonesActivity extends AppCompatActivity {


    public class PhoneListAdapter extends ArrayAdapter<String> {

        public String[] title_list = {"Base Operator","Emergency","On Base Emergency","Ramstein Security Forces","Vogelweh Security Forces","AADD","SARC","LRMC Emergency Room","Suicide Hotline/Help","K-Town Polizei(Police)","Landstuhl Polizei(Police)"};
        public String[] phone_list = {"06371471110","112","0637147112","06371472050","063715366060","015251723356" ,"06371477272","06371868160","06371462390"," 06313692150","0637192290"};

        private Activity activity;

        public PhoneListAdapter (Activity activity, int textViewResourceId) {
            super(activity, textViewResourceId, new ArrayList<String>());
            this.activity = activity;
        }

        public int getCount() {
            return title_list.length;
        }


        @Override
        public String getItem(int position) {
            return title_list[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            EmergencyPhoneCell cell = new EmergencyPhoneCell(activity);
            cell.setTitleAndPhone(title_list[position], phone_list[position]);

            return cell;
        }
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_phones);

        ListView listView = (ListView)findViewById(R.id.listview);

        final PhoneListAdapter adapter = new PhoneListAdapter(this, R.layout.activity_emergency_phones);
        listView.setAdapter( adapter);

        final Activity activity = this;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {



                final BGAAlertController alertController = new BGAAlertController(activity, getResources().getString(R.string.app_name), "Do you want to call " + adapter.title_list[position] + "?", BGAAlertController.AlertControllerStyle.Alert);


                if( position == 5)
                {

                    alertController.addAction(new BGAAlertAction("AADD Facebook", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                        @Override
                        public void onClick() {
                            String url = "https://www.facebook.com/KMCArmedForcesAgainstDrunkDriving/";


                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);

                        }
                    }));



                }



                alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {
                        //showToast("DSN " + dsn_phone);
                    }
                }));
                alertController.addAction(new BGAAlertAction( "Call", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + adapter.phone_list[position]));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);






                            }



                }));








                alertController.addAction(new BGAAlertAction( "Share Contact", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {

                        String content = "Contact information: \n" + adapter.title_list[position] + "\n" + adapter.phone_list[position];

                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, "");
                        i.putExtra(Intent.EXTRA_TEXT, content);
                        try {
                            startActivity(Intent.createChooser(i, "Share via"));
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(activity, "There is no sharing client installed.", Toast.LENGTH_SHORT);
                        }
                    }
                }));

                alertController.setCancelable(false);
                alertController.show();


            }
        });
    }
}
