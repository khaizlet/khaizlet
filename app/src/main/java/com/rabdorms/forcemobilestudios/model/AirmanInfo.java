package com.rabdorms.forcemobilestudios.model;

import android.widget.Spinner;

import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;


public class AirmanInfo {

    public AirmanInfo() {
    }

    @SerializedName("building")
    public String building;

    @SerializedName("dutyPhone")
    public String dutyPhone;

    @SerializedName("fname")
    public String fname;

    @SerializedName("lname")
    public String lname;

    @SerializedName("cell")
    public String cell;

    @SerializedName("room")
    public String room;

    @SerializedName("squadron")
    public String squadron;

    @SerializedName("rankedit")
    public Integer rankedit;


    @SerializedName("rank")
    public Integer rank;

    @SerializedName("gender")
    public Integer gender;

    @SerializedName("base")
    public Integer base;


    public static AirmanInfo g_airman = new AirmanInfo();
}

