package com.rabdorms.forcemobilestudios.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import com.rabdorms.forcemobilestudios.model.AirmanInfo;


import com.alex.common.adapter.viewRepairs;
import com.rabdorms.forcemobilestudios.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.logging.Logger;

import static com.rabdorms.forcemobilestudios.model.AirmanInfo.g_airman;


public class repairResource extends AppCompatActivity  {




        ListView lv_resource;
        ImageView img_home_back;
        viewRepairs list_adapter;
        String[] bldg, room, desc, status;

        Activity activity;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_resource);

            activity = this;
           lv_resource = (ListView) findViewById(R.id.lv_resource);
        //    img_home_back = (ImageView) findViewById(admt.dev.kch_khs.R.id.img_home_back_res);
        //    img_home_back.setOnClickListener(this);

            fetchData process = new fetchData(this);
            process.execute();
        }



        public class fetchData extends AsyncTask<Void,Void,Void> {

            String data = "";
            String[] bldg_temp;
            String[] room_temp;
            String[] desc_temp;
            String[] status_temp;

            int num=0;
            private ProgressDialog dialog;

            public fetchData(repairResource activity) {
                dialog = new ProgressDialog(activity);
            }

            @Override
            protected void onPreExecute() {
                dialog.setMessage("Loading data, please wait...");
                dialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    URL url = new URL("https://script.google.com/macros/s/AKfycbx_bc2VtMIXyWURfzvUxYWqB5AoAKbfgIJK7umucey9IBHdvT0/exec");
                    // https://script.google.com/macros/s/AKfycbwUL9kaUSG_DTLUsnwhGfT_QaECSUHqqvE7Zb8_WqsqJa6V61lj/exec
                    //https://script.google.com/macros/s/AKfycbzLKHCHMVd3VciwauCGQaBAPoy1ItdCBwYzjaY7loB7spBJgp8/exec

                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String line = "";
                    while (line != null) {
                        line = bufferedReader.readLine();
                        if(line != null)
                            data = data + line;
                    }

                    JSONObject jo_original = new JSONObject(data);
                    JSONArray jsonArray = jo_original.getJSONArray("repair");

                    bldg_temp = new String[jsonArray.length()];
                    room_temp = new String[jsonArray.length()];
                    desc_temp = new String[jsonArray.length()];
                    status_temp = new String[jsonArray.length()];


                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject JO = (JSONObject) jsonArray.get(i);
                        if(!(JO.getString("bldg").equals("")|| JO.getString("bldg")==null) ) {
                           if(JO.getString("bldg").equals(g_airman.building) && JO.getString("room").equals(g_airman.room))
                            {

                                bldg_temp[num] = JO.getString("bldg");
                                room_temp[num] = JO.getString("room");
                                desc_temp[num] = JO.getString("desc");
                                status_temp[num] = JO.getString("status");

                                num++;
                            }
                        }
                    }


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                    bldg = new String[num];
                    room = new String[num];
                    desc = new String[num];
                    status = new String[num];



                    for (int i = 0; i < num; i++) {

                        {
                         //   if (bldg_temp[i].equals(g_airman.building)) {



                                bldg[i] = bldg_temp[i];
                                room[i] = room_temp[i];
                                desc[i] = desc_temp[i];
                                status[i] = status_temp[i];
                                //break;
                           // }
                        }






                    }

                System.out.println(g_airman.building.toString());
                System.out.println(bldg.toString());



                    list_adapter = new viewRepairs((repairResource) activity, bldg, room, desc, status);
                    lv_resource.setAdapter(list_adapter);



            }

        }
    }














