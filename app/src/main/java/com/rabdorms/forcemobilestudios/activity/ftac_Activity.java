package com.rabdorms.forcemobilestudios.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alex.common.adapter.ViewPagerAdapter;
import com.alex.common.helper.BottomNavigationViewHelper;
import com.alex.common.utils.UIUtil;
import com.rabdorms.forcemobilestudios.R;
import com.rabdorms.forcemobilestudios.fragment.AtticFragment;
import com.rabdorms.forcemobilestudios.fragment.DFACFragment;
import com.rabdorms.forcemobilestudios.fragment.DRCFragment;
import com.rabdorms.forcemobilestudios.fragment.GymFragment;
import com.rabdorms.forcemobilestudios.fragment.PostalFragment;
import com.rabdorms.forcemobilestudios.fragment.Schedule_Fragment;
import com.rabdorms.forcemobilestudios.fragment.bringToClass;
import com.rabdorms.forcemobilestudios.fragment.ftac_Fragment;
import com.rabdorms.forcemobilestudios.fragment.upcoming_Class;

import java.util.Calendar;
import java.util.Date;

import info.hoang8f.android.segmented.SegmentedGroup;

import static com.rabdorms.forcemobilestudios.model.AirmanInfo.g_airman;
//import com.rabdorms.forcemobilestudios.fragment.GymFragment;


public class ftac_Activity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ftac_main);

        final BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);

        BottomNavigationViewHelper.removeShiftMode(navigation);

        final ViewPager viewPager = (ViewPager)findViewById(R.id.view_pager);
        viewPager.setOffscreenPageLimit(4);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ftac_Fragment(), "FTAC");
        adapter.addFragment(new Schedule_Fragment(), "Schedule");
        adapter.addFragment(new upcoming_Class(), "Upcoming Class");
        adapter.addFragment(new bringToClass(),"Bring to Class");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                int[] itemids = {R.id.FTAC_Menu, R.id.schedule_menu, R.id.upcomingClasses, R.id.notebookMenu};

                navigation.setSelectedItemId(itemids[position]);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        navigation.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.FTAC_Menu:
                        viewPager.setCurrentItem(0);
                        return true;
                    case R.id.schedule_menu:
                        viewPager.setCurrentItem(1);
                        return true;
                    case R.id.upcomingClasses:
                        viewPager.setCurrentItem(2);
                        return true;
                    case R.id.notebookMenu:
                        viewPager.setCurrentItem(3);
                        return true;

                }

                return true;
            }
        });
    }


}
