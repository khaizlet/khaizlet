package com.rabdorms.forcemobilestudios.fragment;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.alex.common.utils.AppUtil;
import com.rabdorms.forcemobilestudios.R;
import com.rabdorms.forcemobilestudios.activity.MainActivity;
import com.rabdorms.forcemobilestudios.activity.MapsActivity;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.Date;

import cn.bingoogolapple.alertcontroller.BGAAlertAction;
import cn.bingoogolapple.alertcontroller.BGAAlertController;
import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * A simple {@link Fragment} subclass.
 */
public class DRCFragment extends Fragment {

    ImageView drcstatus_imageview;
    TextView hoursofoperation_label;

    RecyclerView listView;

    public boolean isKapaun = false;

    public DRCFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_drc, container, false);

        SegmentedGroup segment = (SegmentedGroup) view.findViewById(R.id.segment);

        segment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                LinearLayout schedule_layout = (LinearLayout)view.findViewById(R.id.schedule_layout);
                LinearLayout contactinfo_layout = (LinearLayout)view.findViewById(R.id.contactinfo_layout);

                if( checkedId == R.id.segment_schedule) {
                    schedule_layout.setVisibility(View.VISIBLE);
                    contactinfo_layout.setVisibility(View.GONE);
                } else {
                    schedule_layout.setVisibility(View.GONE);
                    contactinfo_layout.setVisibility(View.VISIBLE);
                }
            }
        });

        if( isKapaun) {
            TextView titleLabel = (TextView)view.findViewById(R.id.title_label);
            titleLabel.setText("Welcome to the Dorm Reception Center");
        }

        initScheduleLayout(view);
        initListViewLayout(view);

        return view;
    }

    void initScheduleLayout( View parentView) {


        drcstatus_imageview = (ImageView)parentView.findViewById(R.id.drc_status_image);
        hoursofoperation_label = (TextView) parentView.findViewById(R.id.hoursofoperation_label);

        Date currentTime = Calendar.getInstance().getTime();

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentTime);

        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 7, 30, 0);
        Date seven_half_today = cal.getTime();

        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 16, 30, 0);
        Date four_half_today = cal.getTime();

        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 9, 0, 0);
        Date nine_monday = cal.getTime();

        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 16, 30, 0);
        Date four_half_monday = cal.getTime();

        String weekday = MainActivity.weekDateFormat.format(currentTime);

        hoursofoperation_label.setText("Monday and Friday\n0900 - 1630\nTuesday through Thursday\n0730 - 1630");

        if(weekday.equals("Saturday") || weekday.equals("Sunday")) {
            drcstatus_imageview.setImageResource(R.drawable.closed_2);
        } else {
            if( weekday.equals("Monday") || weekday.equals("Friday")) {
                if( currentTime.after(nine_monday) && four_half_monday.after(currentTime)) {
                    drcstatus_imageview.setImageResource(R.drawable.open_2);
                } else {
                    drcstatus_imageview.setImageResource(R.drawable.closed_2);
                }
            } else {
                if( currentTime.after(seven_half_today) && four_half_today.after(currentTime)) {
                    drcstatus_imageview.setImageResource(R.drawable.open_2);
                } else {
                    drcstatus_imageview.setImageResource(R.drawable.closed_2);
                }
            }
        }

    }

    void initListViewLayout( View parentView) {

        listView = (RecyclerView) parentView.findViewById(R.id.listview);
        listView.setHasFixedSize(true);
        listView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));


        final ContactInfoListAdapter adapter = new ContactInfoListAdapter( getActivity());
        if( isKapaun) {
            adapter.phone_list = new String[]{"2171", "480-3676", "06371473676", "01735863614", "86ces.cehd.1@us.af.mil", "Share contact list"};
        }
        listView.setAdapter( adapter);
    }

    public interface OnItemClickListener {
        void onItemClick( int position);
    }

    public class ContactInfoListAdapter extends RecyclerView.Adapter<ContactInfoListAdapter.ViewHolder> implements OnItemClickListener {

        public String[] title_list = {"Bldg","DSN","Commercial","ADL On-Call","Email Address","Share Contacts"};
        public String[] phone_list = {"2108","480-3676","06371473676","01735863614","86ces.cehd.1@us.af.mil","Share contact list"};
        public int[] img_ids = {R.mipmap.ic_placeholder, R.mipmap.ic_dnsdrc, R.mipmap.ic_commercialdrc, R.mipmap.ic_adldrc, R.mipmap.ic_maildrc, R.mipmap.ic_sharedrc};

        private Activity activity;

        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            public ImageView image_view;
            public TextView phone_label;
            public TextView title_label;

            public ViewHolder(View view) {
                super(view);
                image_view = (ImageView) view.findViewById(R.id.image_view);
                phone_label = (TextView) view.findViewById(R.id.phone_label);
                title_label = (TextView) view.findViewById(R.id.title_label);
            }
        }

        public ContactInfoListAdapter ( Activity activity) {
            this.activity = activity;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_contact_info, parent, false);
            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            holder.image_view.setImageResource(img_ids[position]);
            holder.title_label.setText(title_list[position]);
            holder.phone_label.setText(phone_list[position]);
            final OnItemClickListener listener = this;
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position);
                }
            });
        }

        @Override
        public int getItemCount() {
            return title_list.length;
        }


        @Override
        public void onItemClick(final int position) {

            if(position == 0) {

                if (isKapaun){

                    final BGAAlertController alertController = new BGAAlertController(activity, getResources().getString(R.string.app_name), "Would you like to visit the Kapaun DRC?", BGAAlertController.AlertControllerStyle.Alert);
                    alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                        @Override
                        public void onClick() {
                        }
                    }));



                    alertController.addAction(new BGAAlertAction("KAPAUN DRC", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                        @Override
                        public void onClick() {


                            AppUtil.openGoogleMap(activity, "49.427513", "7.700167");
                            // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                            String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.427513 + "," + 7.700167 + " (" + "KAPAUN DRC" + ")";
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                            //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                            startActivity(intent);




                        }
                    }));
                    alertController.setCancelable(false);
                    alertController.show();



                }
                else{
                final BGAAlertController alertController = new BGAAlertController(activity, getResources().getString(R.string.app_name), "Would you like to visit the DRC?", BGAAlertController.AlertControllerStyle.Alert);
                alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {
                    }
                }));



                alertController.addAction(new BGAAlertAction("DRC", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {


                        AppUtil.openGoogleMap(activity, "49.445552", "7.604984");
                        // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                        String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.445552 + "," + 7.604984 + " (" + "DRC" + ")";
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                        //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                        startActivity(intent);


                    }
                 }));
                alertController.setCancelable(false);
                alertController.show();}



            } else if(position == 1) {

                final BGAAlertController alertController = new BGAAlertController(activity, getResources().getString(R.string.app_name), "Domitory Reception Center\nDSN phone number\n" + phone_list[position], BGAAlertController.AlertControllerStyle.Alert);

                alertController.addAction(new BGAAlertAction("Dismiss", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {
                    }
                }));
                alertController.setCancelable(false);
                alertController.show();

            } else if( (position == 2) || (position == 3)) {

                final BGAAlertController alertController = new BGAAlertController(activity, getResources().getString(R.string.app_name), "Do you want to call "+title_list[position]+"?", BGAAlertController.AlertControllerStyle.Alert);

                alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {
                    }
                }));
                alertController.addAction(new BGAAlertAction( "Call", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {

                        AppUtil.callPhone( activity, phone_list[position]);

                    }
                }));
                alertController.setCancelable(false);
                alertController.show();

            } else if( position == 4) {

                AppUtil.mailTo(activity, "help.forcemobilestudios@gmail.com", "", "");

            } else if( position == 5) {

                String content = "Contact information:\nBldg. 2108\nDSN: 480-3676\nCommercial 06371473676\nADL On-Call 01735863614\nEmail Address: 86ces.cehd.1@us.af.mil";

                AppUtil.shareText(activity, content);
            }
        }

    }
}
