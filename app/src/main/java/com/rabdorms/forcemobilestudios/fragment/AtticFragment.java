package com.rabdorms.forcemobilestudios.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.alex.common.utils.AppUtil;
import com.rabdorms.forcemobilestudios.R;
import com.rabdorms.forcemobilestudios.activity.MapsActivity;

import cn.bingoogolapple.alertcontroller.BGAAlertAction;
import cn.bingoogolapple.alertcontroller.BGAAlertController;


/**
 * A simple {@link Fragment} subclass.
 */
public class AtticFragment extends Fragment implements View.OnClickListener {

    Button address_button;
    Button email_button;
    Button facebook_button;

    public AtticFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_attic, container, false);

        address_button = (Button)view.findViewById(R.id.address_button);
        email_button = (Button)view.findViewById(R.id.email_button);
        facebook_button = (Button)view.findViewById(R.id.facebook_button);

        address_button.setOnClickListener(this);
        email_button.setOnClickListener(this);
        facebook_button.setOnClickListener(this);
        // Inflate the layout for this fragment
        return view;
    }


    @Override
    public void onClick(final View v) {

        if( v == address_button) {

            String msg = "Would you like to visit the Airman's Attic?";
            String lat = "49.447971";
            String lon = "7.606595";
            String mark_title = "Airman's Attic Bdlg. 2126";


            final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "Would you like to visit the Airman's Attic?", BGAAlertController.AlertControllerStyle.Alert);
            alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                @Override
                public void onClick() {
                }
            }));


            alertController.addAction(new BGAAlertAction( "AIRMAN'S ATTIC", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                @Override
                public void onClick() {

                    AppUtil.openGoogleMap(getActivity(), "49.447971", "7.606595");

                    // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                    String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.447971 + "," + 7.606595 + " (" + "AIRMAN'S ATTIC" + ")";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                    //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                    startActivity(intent);



                }
            }));
            alertController.setCancelable(false);
            alertController.show();



        } else if( v == email_button){

            final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "Do you want to email the Airman's Attic?", BGAAlertController.AlertControllerStyle.Alert);

            alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                @Override
                public void onClick() {
                }
            }));

            alertController.addAction(new BGAAlertAction( "Email", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                @Override
                public void onClick() {
                    AppUtil.mailTo(getActivity(), "kmcairmansattic@gmail.com", "", "");
                }
            }));


            alertController.setCancelable(false);
            alertController.show();

        } else if( v == facebook_button){

            final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "Do you want to open the Airman's Attic Facebook page?", BGAAlertController.AlertControllerStyle.Alert);

            alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                @Override
                public void onClick() {
                }
            }));

            alertController.addAction(new BGAAlertAction( "Facebook", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                @Override
                public void onClick() {

                    try {
                        getContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/256308841120375"));
                    } catch (Exception e) {
                        AppUtil.openUrl(getActivity(), "https://www.facebook.com/KMCAirmansAttic");
                        //return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/<user_name_here>"));
                    }

                }
            }));


            alertController.setCancelable(false);
            alertController.show();

        }
    }

}
