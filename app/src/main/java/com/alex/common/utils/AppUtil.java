package com.alex.common.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.MapStyleOptions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;



public class AppUtil {

    static public Context applicationContext;

    static public void removeSettingsString(String key) {

        if (applicationContext != null) {
            SharedPreferences preferences = applicationContext.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.remove(key);
            editor.commit();
        }
    }

    static public void setSettingsString(String value, String key) {

        if (applicationContext != null) {
            SharedPreferences preferences = applicationContext.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(key, value);
            editor.commit();
        }
    }

    static public String getSettingsString(String key) {
        if (applicationContext != null) {
            SharedPreferences preferences = applicationContext.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
            return preferences.getString(key, null);
        }
        return null;
    }

    //================================================

    static public boolean isFirstLaunch() {
        if( getSettingsString("IS_APP_FIRST_LAUNCH") == null) {
            return true;
        }
        //return true;
        return false;
    }

    static SimpleDateFormat AppUtil_hourDateFormat = new SimpleDateFormat("H");

    static public String greeting() {

        Date currentTime = Calendar.getInstance().getTime();

        Integer hour = Integer.parseInt( AppUtil_hourDateFormat.format(currentTime));

        if ((hour >= 0) && (hour < 12)) {
            return "Good Morning";
        } else if( hour >= 12 && hour < 17) {
            return "Good Afternoon";
        } else if( hour >= 17) {
            return "Good Evening";
        }
        return "";
    }

    static public Integer getIndexOf( int[] array, int value) {

        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                return i;
            }
        }

        return -1;
    }

    static public void shareText( Activity activity, String content) {

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "");
        i.putExtra(Intent.EXTRA_TEXT, content);
        try {
            activity.startActivity(Intent.createChooser(i, "Share via"));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(activity, "There is no sharing client installed.", Toast.LENGTH_SHORT);
        }

    }


    static public void openUrl( Activity activity, String url) {

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

        try {
            activity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(activity, "no email app is available", Toast.LENGTH_SHORT);
            //TODO: Handle case where no email app is available
        }

    }

    static public void mailTo( Activity activity, String mail, String subject, String body) {

        String mailto = "mailto:" + mail +
                "?subject=" + Uri.encode(subject) +
                "&body=" + Uri.encode(body);

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse(mailto));

        try {
            activity.startActivity(emailIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(activity, "no email app is available", Toast.LENGTH_SHORT);
            //TODO: Handle case where no email app is available
        }

    }

    static public void callPhone( Activity activity, String phone) {

        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone.replace(" ", "")));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);

    }

    static public void openGoogleMap( Activity activity, String lat, String lon) {

        Uri webpage = Uri.parse("geo:" + lat + "," + lon);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        activity.startActivity(intent);


    }
}
