package com.alex.common.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rabdorms.forcemobilestudios.R;
import com.rabdorms.forcemobilestudios.activity.RepairRequestActivity;
import com.rabdorms.forcemobilestudios.activity.repairResource;

import static com.rabdorms.forcemobilestudios.model.AirmanInfo.g_airman;

public class viewRepairs extends BaseAdapter {
    String[] bldg, room, desc, status;
    Context context;
    private static LayoutInflater inflater = null;

    public viewRepairs(repairResource resourceActivity, String[] bldg, String[] room, String[] desc, String[] status) {
        // TODO Auto-generated constructor stub
        this.bldg = bldg;
        this.room = room;
        this.desc = desc;
        this.status = status;
        context = resourceActivity;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return bldg.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    public class Holder {
        TextView tv_res_title, tv_res_info, tv_res_domain;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub


            Holder holder = new Holder();
            View view;
            view = inflater.inflate(R.layout.listitem_resource, null);
            holder.tv_res_title = (TextView) view.findViewById(R.id.tv_res_title);
            holder.tv_res_info = (TextView) view.findViewById(R.id.tv_res_info);
            holder.tv_res_domain = (TextView) view.findViewById(R.id.tv_res_domain);

            holder.tv_res_title.setText("Building: " + bldg[position]+ "  " + "Room:"+ room[position]);


            holder.tv_res_domain.setText("Description: " + desc[position]);
            holder.tv_res_info.setText("Status: " + status[position]);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    //    Toast.makeText(context, "You Clicked " + result[position], Toast.LENGTH_LONG).show();
                }
            });
            if (bldg[position] == null)
                view.setVisibility(View.INVISIBLE);
            return view;
        }


}














