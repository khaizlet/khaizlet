package com.rabdorms.forcemobilestudios.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.alex.common.utils.AppUtil;
import com.rabdorms.forcemobilestudios.R;


import cn.bingoogolapple.alertcontroller.BGAAlertAction;
import cn.bingoogolapple.alertcontroller.BGAAlertController;
import info.hoang8f.android.segmented.SegmentedGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class PostalFragment extends Fragment implements View.OnClickListener {
    long a = Long.parseLong("06315367866");
    String ss = String.valueOf(a);

    Button pickup_phone_button;
    Button pickup_addr_button;
    Button ship_phone_button;
    Button ship_addr_button;
    Button kapaun_mail;
    Button kapaun_call_mail;

    public boolean isKapaun = false;
    public int checkedId;

    public PostalFragment() {
        // Required empty public constructor
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(isKapaun ? R.layout.fragment_postal_kapaun : R.layout.fragment_postal, container, false);

        if (isKapaun == false) {

            SegmentedGroup segment = (SegmentedGroup) view.findViewById(R.id.segment);

            segment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    LinearLayout mail_pickup_layout = (LinearLayout) view.findViewById(R.id.mail_pickup_layout);
                    LinearLayout mail_ship_layout = (LinearLayout) view.findViewById(R.id.mail_ship_layout);

                    if (checkedId == R.id.segment_mail_pickup) {
                        mail_pickup_layout.setVisibility(View.VISIBLE);
                        mail_ship_layout.setVisibility(View.GONE);


                    } else {
                        mail_pickup_layout.setVisibility(View.GONE);
                        mail_ship_layout.setVisibility(View.VISIBLE);


                    }
                }
            });

        }



            if(isKapaun == false) {

                pickup_phone_button = (Button) view.findViewById(R.id.mail_pickup_phone);
                pickup_addr_button = (Button) view.findViewById(R.id.mail_pickup_address);
                ship_phone_button = (Button) view.findViewById(R.id.mail_ship_phone);
                ship_addr_button = (Button) view.findViewById(R.id.mail_ship_address);

                pickup_phone_button.setOnClickListener(this);
                pickup_addr_button.setOnClickListener(this);
                ship_phone_button.setOnClickListener(this);
                ship_addr_button.setOnClickListener(this);
            }






        if (isKapaun == true)

        {
            kapaun_mail = (Button) view.findViewById(R.id.south_gym);
            kapaun_mail.setOnClickListener(this);
            kapaun_call_mail = (Button) view.findViewById(R.id.kapaun_call_mail);
            kapaun_call_mail.setOnClickListener(this);


        }


        // Inflate the layout for this fragment
        return view;
    }


    public void onClick(final View v) {


        if (isKapaun) {
            {

                if (v == kapaun_call_mail) {


                    final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "Do you want to call Kapaun Mail?", BGAAlertController.AlertControllerStyle.Alert);

                    alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                        @Override
                        public void onClick() {
                        }
                    }));
                    alertController.addAction(new BGAAlertAction("Call", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                        @Override
                        public void onClick() {
                            AppUtil.callPhone(getActivity(), ss);
                        }
                    }));
                    alertController.setCancelable(false);
                    alertController.show();


                }
            }


        }


        {
            if ((v == pickup_phone_button) || (v == ship_phone_button)) {

                String msg = "Do you want to call the Post Office?";

                final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), msg, BGAAlertController.AlertControllerStyle.Alert);

                alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {


                    }
                }));
                alertController.addAction(new BGAAlertAction("Call", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {

                        AppUtil.callPhone(getActivity(), ((Button) v).getText().toString());


                    }
                }));
                alertController.setCancelable(false);
                alertController.show();

            } else {

                // isKapaun = true;

                if (isKapaun) {

                    if (v == kapaun_mail) {

                        final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "Would you like to Ship Mail?", BGAAlertController.AlertControllerStyle.Alert);
                        alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                            @Override
                            public void onClick() {
                            }
                        }));


                        alertController.addAction(new BGAAlertAction("Kapaun Mail", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                            @Override
                            public void onClick() {

                                AppUtil.openGoogleMap(getActivity(), "49.427113", "7.00738");

                                // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                                String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.427113 + "," + 7.700738 + " (" + "MAIL AND SHIP" + ")";
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                                //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                                startActivity(intent);


                            }
                        }));
                        alertController.setCancelable(false);
                        alertController.show();


                    }

                }


            }





            if (v == ship_addr_button) {
                //   if (checkedId == R.id.segment_mail_ship) {

                final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "Would you like to Ship Mail?", BGAAlertController.AlertControllerStyle.Alert);
                alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {
                    }
                }));


                alertController.addAction(new BGAAlertAction("Would you like to ship mail?", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {

                        AppUtil.openGoogleMap(getActivity(), "49.445513", "7.601741");

                        // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                        String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.445513 + "," + 7.601741 + " (" + "MAIL AND SHIP" + ")";
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                        //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                        startActivity(intent);


                    }
                }));
                alertController.setCancelable(false);
                alertController.show();


            }


            if (v == pickup_addr_button) {


                final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "PICK UP", BGAAlertController.AlertControllerStyle.Alert);
                alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {
                    }
                }));


                alertController.addAction(new BGAAlertAction("Would you like to Pick Up Mail?", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {

                        AppUtil.openGoogleMap(getActivity(), "49.445926", "7.602468");

                        // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                        String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.445926 + "," + 7.602468 + " (" + "MAIL PICK UP" + ")";
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                        //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                        startActivity(intent);


                    }
                }));
                alertController.setCancelable(false);
                alertController.show();


            }



        }


    }
}



