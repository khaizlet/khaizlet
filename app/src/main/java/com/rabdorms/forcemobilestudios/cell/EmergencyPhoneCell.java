package com.rabdorms.forcemobilestudios.cell;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rabdorms.forcemobilestudios.R;



public class EmergencyPhoneCell extends LinearLayout {

    String title;
    String phone;

    TextView title_label;
    TextView phone_label;

    public EmergencyPhoneCell(Context context) {
        super(context);
        init();
    }

    public EmergencyPhoneCell(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EmergencyPhoneCell(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EmergencyPhoneCell(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private void init() {
        inflate(getContext(), R.layout.cell_emergency_phone, this);
        title_label = (TextView) findViewById(R.id.title_label);
        phone_label = (TextView) findViewById(R.id.phone_label);
    }

    public void setTitleAndPhone(String title, String phone) {
        title_label.setText(title);
        phone_label.setText(phone);
    }

}
