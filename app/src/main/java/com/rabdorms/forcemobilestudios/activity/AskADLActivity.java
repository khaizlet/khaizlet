package com.rabdorms.forcemobilestudios.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alex.common.utils.UIUtil;
import com.rabdorms.forcemobilestudios.R;

import java.util.Date;
import java.util.Calendar;

import info.hoang8f.android.segmented.SegmentedGroup;

import static com.rabdorms.forcemobilestudios.model.AirmanInfo.g_airman;

public class AskADLActivity extends AppCompatActivity {


    TextView name_editor;
    TextView squadron_editor;
    TextView phonenumber_editor;
    TextView building_editor;
    TextView room_editor;

    EditText question_editor;

    SegmentedGroup rank_segment;
    SegmentedGroup gender_segment;
    SegmentedGroup base_segment;
    SegmentedGroup type_segment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_adl);


        initialize();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_repair_request, menu);
        return true;//super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.repair_request_submit) {

            submitRequest();
        }

        return true;
    }

    String rank_value;
    String gender_value;
    String base_value;
    String type_value;
    Spinner rank_editor;

    void initialize() {

        LinearLayout domitory_layout = (LinearLayout)findViewById(R.id.dormitory_layout);
        UIUtil.setEnableAllViews(false, domitory_layout);
        //domitory_layout.setFocusable(false);
        LinearLayout personal_layout = (LinearLayout)findViewById(R.id.sgt_layout);
        UIUtil.setEnableAllViews(false, personal_layout);

        rank_editor = (Spinner) findViewById(R.id.rank_editor);

        name_editor = (TextView)findViewById(R.id.name_editor);
        squadron_editor = (TextView)findViewById(R.id.squadron_editor);
        phonenumber_editor = (TextView)findViewById(R.id.phone_editor);
        building_editor = (TextView)findViewById(R.id.buildingnumber_editor);
        room_editor = (TextView)findViewById(R.id.roomnumber_editor);

        question_editor = (EditText)findViewById(R.id.question_editor);

        rank_segment = (SegmentedGroup)findViewById(R.id.rank_segment);
        gender_segment = (SegmentedGroup)findViewById(R.id.gender_segment);
        base_segment = (SegmentedGroup)findViewById(R.id.base_segment);
        type_segment = (SegmentedGroup)findViewById(R.id.type_segment);
        //int selected_id = rankSegment.getCheckedRadioButtonId();
//        ((RadioButton)findViewById(R.id.rank_option_ab)).setChecked(true);
        ((RadioButton)findViewById(R.id.gender_option_male)).setChecked(true);
        ((RadioButton)findViewById(R.id.base_option_ramstein)).setChecked(true);
        ((RadioButton)findViewById(R.id.type_new)).setChecked(true);

        if( g_airman == null) return;
        if( g_airman.fname == null) return;


        Spinner spinner = (Spinner) findViewById(R.id.rank_editor);
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(this, R.layout.spinner_style,
                getResources().getStringArray(R.array.Ranks));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(myAdapter);


        rank_editor.setSelection(g_airman.rankedit);

        name_editor.setText(g_airman.fname);
        phonenumber_editor.setText(g_airman.dutyPhone);
        squadron_editor.setText(g_airman.squadron);
        building_editor.setText(g_airman.building);
        room_editor.setText(g_airman.room);

        int[] rank_ids = {R.id.Resident, R.id.Supervisor, R.id.Sgt};
        int[] gender_ids = {R.id.gender_option_male, R.id.gender_option_female};
        int[] base_ids = {R.id.base_option_ramstein, R.id.base_option_kapaun};
        int[] urgency_ids = {R.id.urgency_routine, R.id.urgency_urgent, R.id.urgency_emergency};

        RadioButton radioButton;
        radioButton = (RadioButton)findViewById(rank_ids[g_airman.rank]);
        radioButton.setChecked(true);
        rank_value = radioButton.getText().toString();

        radioButton = (RadioButton)findViewById(gender_ids[g_airman.gender]);
        radioButton.setChecked(true);
        gender_value = radioButton.getText().toString();

        radioButton = (RadioButton)findViewById(base_ids[g_airman.base]);
        radioButton.setChecked(true);
        base_value = radioButton.getText().toString();

        type_value = "New";
    }

    void submitRequest() {

        if(question_editor.getText().toString().isEmpty()) {
            question_editor.setError("Please enter your question.");
            return;
        }

        rank_editor = (Spinner) findViewById(R.id.rank_editor);


        RadioButton radioButton;
        radioButton = (RadioButton)findViewById(rank_segment.getCheckedRadioButtonId());
        rank_value = radioButton.getText().toString();

        radioButton = (RadioButton)findViewById(gender_segment.getCheckedRadioButtonId());
        gender_value = radioButton.getText().toString();

        radioButton = (RadioButton)findViewById(base_segment.getCheckedRadioButtonId());
        base_value = radioButton.getText().toString();

        radioButton = (RadioButton)findViewById(type_segment.getCheckedRadioButtonId());
        type_value = radioButton.getText().toString();

        RadioButton levelbtn = (RadioButton) findViewById(R.id.type_new);
        levelbtn.setChecked(true);

        Date currentTime = Calendar.getInstance().getTime();
        String date_value = MainActivity.dateDateFormat.format(currentTime);
        String time_value = MainActivity.timeDateFormat.format(currentTime);

        String subject = "Ask an ADL - Bldg: " + building_editor.getText().toString() + " - Room: " + room_editor.getText().toString() + " - " + base_value;

        String body = "Rank: " + rank_value + " \n Name: " + name_editor.getText().toString() + "\n Gender: " + gender_value + " \n Squadron: " + squadron_editor.getText().toString() + " \n Duty Phone: " + phonenumber_editor.getText().toString() + "\n Base: " + base_value + "\n Building Number: " + building_editor.getText().toString() + "\n Room Number: " + room_editor.getText().toString() + " \n\n Date: " + date_value + " \n Time: " + time_value + " \n\n Question Category: " + type_value + " \n Question/Concern: " + question_editor.getText().toString();

        String mailto = "mailto:86ces.cehd.1@us.af.mil" +
                "?subject=" + Uri.encode(subject) +
                "&body=" + Uri.encode(body);

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse(mailto));

        try {
            startActivity(emailIntent);
            finish();
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "no email app is available", Toast.LENGTH_SHORT);
            //TODO: Handle case where no email app is available
        }
    }

}