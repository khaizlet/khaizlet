package com.rabdorms.forcemobilestudios.activity;



import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.alex.common.adapter.ViewPagerAdapter;
import com.alex.common.helper.BottomNavigationViewHelper;
import com.rabdorms.forcemobilestudios.R;
import com.rabdorms.forcemobilestudios.fragment.AtticFragment;
import com.rabdorms.forcemobilestudios.fragment.DFACFragment;
import com.rabdorms.forcemobilestudios.fragment.DRCFragment;

import com.rabdorms.forcemobilestudios.fragment.GymFragment;
import com.rabdorms.forcemobilestudios.fragment.PostalFragment;
//import com.rabdorms.forcemobilestudios.fragment.GymFragment;
import com.rabdorms.forcemobilestudios.fragment.Support_Calendar;
import com.rabdorms.forcemobilestudios.fragment.Force_Support_it;
import android.webkit.WebView;



public class Support_Squadron extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_squadron);

        final BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);

        BottomNavigationViewHelper.removeShiftMode(navigation);

        final ViewPager viewPager = (ViewPager)findViewById(R.id.view_pager);
        viewPager.setOffscreenPageLimit(2);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new Force_Support_it(), "86th Force Support");
        adapter.addFragment(new Support_Calendar(), "Events Calendar");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                int[] itemids = {R.id.Support_Event, R.id.Force_Support_it};

                navigation.setSelectedItemId(itemids[position]);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.Support_Event:
                        viewPager.setCurrentItem(0);
                        return true;
                    case R.id.Force_Support_it:
                        viewPager.setCurrentItem(1);

                        return true;


                }


                return true;
            }
        });
    }











    }
