package com.rabdorms.forcemobilestudios.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.alex.common.utils.AppUtil;
import com.rabdorms.forcemobilestudios.R;
import com.rabdorms.forcemobilestudios.activity.MapsActivity;

import cn.bingoogolapple.alertcontroller.BGAAlertAction;
import cn.bingoogolapple.alertcontroller.BGAAlertController;
import info.hoang8f.android.segmented.SegmentedGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class DFACFragment extends Fragment implements View.OnClickListener {

    Button dfac_civ_button;
    Button dfac_addr_button;
    Button jawborn_civ_button;
    Button jawborn_addr_button;
    Button kapaun_dfac;
    Button kapaun_civ;

    public boolean isKapaun = false;


    public DFACFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(isKapaun ? R.layout.fragment_dfac_kapaun : R.layout.fragment_dfac, container, false);

        if (isKapaun == false) {


            SegmentedGroup segment = (SegmentedGroup) view.findViewById(R.id.segment);

            segment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    LinearLayout schedule_layout = (LinearLayout) view.findViewById(R.id.schedule_layout);
                    LinearLayout dfacinfo_layout = (LinearLayout) view.findViewById(R.id.dfac_info_layout);

                    if (checkedId == R.id.segment_schedule) {
                        schedule_layout.setVisibility(View.VISIBLE);
                        dfacinfo_layout.setVisibility(View.GONE);
                    } else {
                        schedule_layout.setVisibility(View.GONE);
                        dfacinfo_layout.setVisibility(View.VISIBLE);
                    }
                }
            });

        }

        jawborn_civ_button = (Button) view.findViewById(R.id.jawborn_civ_button);
        jawborn_addr_button = (Button) view.findViewById(R.id.jawborn_addr_button);


        if (isKapaun == true)

        {
            kapaun_dfac = (Button) view.findViewById(R.id.kapaun_dfac);
            kapaun_dfac.setOnClickListener(this);
            kapaun_civ = (Button) view.findViewById(R.id.kapaun_civ);
            kapaun_civ.setOnClickListener(this);


        }


        if (isKapaun == false) {
            dfac_civ_button = (Button) view.findViewById(R.id.dfac_civ_button);
            dfac_addr_button = (Button) view.findViewById(R.id.dfac_addr_button);
            dfac_civ_button.setOnClickListener(this);
            dfac_addr_button.setOnClickListener(this);

            jawborn_civ_button.setOnClickListener(this);
            jawborn_addr_button.setOnClickListener(this);
        }
        // Inflate the layout for this fragment
        return view;
    }


    //seperate ramstein layout from kpaun

    @Override
    public void onClick(final View v) {


        if (isKapaun) {

            if (v == kapaun_civ) {


                final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "Do you want to call Kapaun Dfac?", BGAAlertController.AlertControllerStyle.Alert);

                alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {
                    }
                }));
                alertController.addAction(new BGAAlertAction("Call", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {
                        AppUtil.callPhone(getActivity(), ((Button) v).getText().toString());
                    }
                }));
                alertController.setCancelable(false);
                alertController.show();


            }


        }


        if ((v == dfac_civ_button) || (v == jawborn_civ_button))


        {

            String msg = "Do you want to call DFAC?";

            if (v == jawborn_civ_button) {
                msg = "Do you want to call it Flight Kitchen?";
            }

            final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), msg, BGAAlertController.AlertControllerStyle.Alert);

            alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                @Override
                public void onClick() {
                }
            }));
            alertController.addAction(new BGAAlertAction("Call", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                @Override
                public void onClick() {
                    AppUtil.callPhone(getActivity(), ((Button) v).getText().toString());
                }
            }));
            alertController.setCancelable(false);
            alertController.show();

        } else {


            if (v == dfac_addr_button) {


                final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "Would you like to visit the Ramstein DFAC?", BGAAlertController.AlertControllerStyle.Alert);
                alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {
                    }
                }));


                alertController.addAction(new BGAAlertAction("DFAC", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {

                        AppUtil.openGoogleMap(getActivity(), "49.445606", "7.603118");

                        // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                        String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.445606 + "," + 7.603118 + " (" + "RAMSTEIN DFAC" + ")";
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                        //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                        startActivity(intent);


                    }
                }));
                alertController.setCancelable(false);
                alertController.show();


            }


            //  String msg = "Would you like to visit the DFAC?";
            // String lat = "49.445757";
            //String lon = "7.602704";
            //String mark_title = "DFAC";

            if (isKapaun)

            {
                if (v == kapaun_dfac) {

                    final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "Would you like to visit the KAPAUN DFAC?", BGAAlertController.AlertControllerStyle.Alert);
                    alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                        @Override
                        public void onClick() {
                        }
                    }));


                    alertController.addAction(new BGAAlertAction("KAPAUN DFAC", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                        @Override
                        public void onClick() {

                            AppUtil.openGoogleMap(getActivity(), "49.427113", "7.700738");

                            // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                            String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.427113 + "," + 7.700738 + " (" + "KAPAUN DFAC" + ")";
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                            //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                            startActivity(intent);


                        }
                    }));
                    alertController.setCancelable(false);
                    alertController.show();


                }
            }

            if (v == jawborn_addr_button) {


                final BGAAlertController alertController = new BGAAlertController(getActivity(), getResources().getString(R.string.app_name), "Would you like to visit the Flight Kitchen?", BGAAlertController.AlertControllerStyle.Alert);
                alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {
                    }
                }));


                alertController.addAction(new BGAAlertAction("FLIGHT KITCHEN", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                    @Override
                    public void onClick() {

                        AppUtil.openGoogleMap(getActivity(), "49.443399", "7.590339");

                        // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                        String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.443399 + "," + 7.590339 + " (" + "FLIGHT KITCHEN" + ")";
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                        //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                        startActivity(intent);


                    }
                }));
                alertController.setCancelable(false);
                alertController.show();


            }
        }
    }
}
