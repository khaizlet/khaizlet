package com.alex.common.app;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;

import com.alex.common.utils.AppUtil;
import com.alex.common.utils.UIUtil;
import com.facebook.drawee.backends.pipeline.Fresco;



public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Fresco.initialize(getApplicationContext());

        AppUtil.applicationContext = getApplicationContext();
        UIUtil.applicationContext = getApplicationContext();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void startActivity(Intent intent, Bundle options) {
        super.startActivity(intent, options);
    }

    @Override
    public void startActivities(Intent[] intents) {
        super.startActivities(intents);
    }

    @Override
    public void startActivities(Intent[] intents, Bundle options) {
        super.startActivities(intents, options);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }


}
