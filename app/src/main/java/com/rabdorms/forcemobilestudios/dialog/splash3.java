package com.rabdorms.forcemobilestudios.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;

import com.alex.common.utils.AppUtil;
import com.daimajia.androidanimations.library.Techniques;
import com.rabdorms.forcemobilestudios.R;
import com.rabdorms.forcemobilestudios.activity.ProfileEditActivity;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;






    public class splash3  extends AwesomeSplash {
        static Activity activity;
        AlertDialog dialog;

        //DO NOT OVERRIDE onCreate()!
        //if you need to start some services do it in initSplash()!

        public static void show(Activity activity) {
            com.rabdorms.forcemobilestudios.dialog.splash.activity = activity;
            com.rabdorms.forcemobilestudios.dialog.splash dialog = new com.rabdorms.forcemobilestudios.dialog.splash();
        }


        @Override
        public void initSplash(ConfigSplash configSplash) {
            /* you don't have to override every property */

            ;

            //Customize Circular Reveal
            configSplash.setBackgroundColor(R.color.LightBlue); //any color you want form colors.xml
            configSplash.setAnimCircularRevealDuration(2000); //int ms
            configSplash.setRevealFlagX(Flags.REVEAL_RIGHT);  //or Flags.REVEAL_LEFT
            configSplash.setRevealFlagY(Flags.REVEAL_BOTTOM); //or Flags.REVEAL_TOP

            //Choose LOGO OR PATH; if you don't provide String value for path it's logo by default

            //Customize Logo
            configSplash.setLogoSplash(R.mipmap.img_welcome03); //or any other drawable
            configSplash.setAnimLogoSplashDuration(2000); //int ms
            configSplash.setAnimLogoSplashTechnique(Techniques.Landing);


            //choose one form Techniques (ref: https://github.com/daimajia/AndroidViewAnimations)


            //Customize Path
            //configSplash.setPathSplash(Constants.DROID_LOGO); //set path String

            configSplash.setOriginalHeight(400); //in relation to your svg (path) resource
            configSplash.setOriginalWidth(400); //in relation to your svg (path) resource
            configSplash.setAnimPathStrokeDrawingDuration(3000);
            configSplash.setPathSplashStrokeSize(3); //I advise value be <5
            configSplash.setPathSplashStrokeColor(R.color.accent); //any color you want form colors.xml
            configSplash.setAnimPathFillingDuration(3000);
            configSplash.setPathSplashFillColor(R.color.Wheat); //path object filling color


            //Customize Title
            configSplash.setTitleSplash("CREATE YOUR ACCOUNT");
            configSplash.setTitleTextColor(R.color.black);

            configSplash.setTitleTextSize(20); //float value
            configSplash.setAnimTitleDuration(3000);
            configSplash.setAnimTitleTechnique(Techniques.Landing);
            configSplash.setTitleFont("fonts/Montserrat-Black.ttf"); //provide string to your font located in assets/fonts/


        }

        @Override
        public void animationsFinished() {
            finish();
          //  AppUtil.setSettingsString("true", "IS_APP_FIRST_LAUNCH");
            Intent intent = new Intent(this, ProfileEditActivity.class);
            this.startActivity(intent);

        }


    }















