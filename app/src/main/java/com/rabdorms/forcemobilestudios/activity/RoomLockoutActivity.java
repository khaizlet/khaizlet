package com.rabdorms.forcemobilestudios.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.View.OnClickListener;

import com.alex.common.utils.AppUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.rabdorms.forcemobilestudios.R;

import cn.bingoogolapple.alertcontroller.BGAAlertAction;
import cn.bingoogolapple.alertcontroller.BGAAlertController;
import info.hoang8f.android.segmented.SegmentedGroup;







public class RoomLockoutActivity extends AppCompatActivity {






    private GoogleMap mMap;
















    @Override
    protected void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_lockout);





        //image = (ImageView) findViewById(R.id.imageView5);
       // button = (Button) findViewById(R.id.open_ram_key);




        SegmentedGroup base_segment = (SegmentedGroup)findViewById(R.id.base_segment);
        final TextView bldg_label = (TextView)findViewById(R.id.bldg_label);

        base_segment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {


                if( checkedId == R.id.base_option_kapaun) {
                    bldg_label.setText("Bldg. 2818");

                    Button button = (Button) findViewById(R.id.open_ram_key);
                    button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            ImageView myImageView = (ImageView)findViewById(R.id.imageView7);
                            myImageView.setImageResource(R.drawable.k_key);
                            if (myImageView.getVisibility() == View.GONE) {
                                myImageView.setVisibility(View.VISIBLE);
                            }else
                            {
                                myImageView.setVisibility(View.GONE);
                            }


                            // Do something in response to button click
                        }
                    });


                } else if( checkedId == R.id.base_option_ramstein) {
                    bldg_label.setText("Bldg. 2112");
                    Button button = (Button) findViewById(R.id.open_ram_key);
                    button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            ImageView myImageView = (ImageView)findViewById(R.id.imageView7);
                            myImageView.setImageResource(R.drawable.ram_key);
                             if (myImageView.getVisibility() == View.GONE) {
                                myImageView.setVisibility(View.VISIBLE);
                            }else
                            {
                                myImageView.setVisibility(View.GONE);
                            }


                            // Do something in response to button click
                        }
                    });
                }
            }
        });











        Button openMapButton = (Button)findViewById(R.id.open_map_button);
        openMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMap();
            }
        });

        Button buttonit = (Button) findViewById(R.id.open_ram_key);
        buttonit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView myImageView = (ImageView)findViewById(R.id.imageView7);
                myImageView.setImageResource(R.drawable.ram_key);

                if (myImageView.getVisibility() == View.GONE) {
                    myImageView.setVisibility(View.VISIBLE);
                }else
                {
                    myImageView.setVisibility(View.GONE);
                }

            }
        });





//
//        final BGAAlertController alertController = new BGAAlertController(this, "ATTENTION", "Residents are only authorized to sign out their assigned room key for a duration not to exceed 30 min.\n Members who fail to comply will be punished in accordance with Article 92 of the UCMJ: Failure to Obey Order or Regulations", BGAAlertController.AlertControllerStyle.Alert);
//
//        alertController.addAction(new BGAAlertAction("Dismiss", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
//            @Override
//            public void onClick() {
//                //showToast("DSN " + dsn_phone);
//            }
//        }));
//
//        alertController.setCancelable(false);
//        alertController.show();


        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.dialog_room_lockout_attention, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        final AlertDialog dialog = alertDialogBuilder.create();
        dialog.setView(view);

        final Button dismissButton = (Button) view.findViewById(R.id.dismiss_button);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    void openMap() {

        final Activity activity = this;

        final BGAAlertController alertController = new BGAAlertController(this, "KMC Dorms", "Would you like to visit the KeyTrak kiosk?", BGAAlertController.AlertControllerStyle.Alert);

        alertController.addAction(new BGAAlertAction("Cancel", BGAAlertAction.AlertActionStyle.Cancel, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {
                //showToast("DSN " + dsn_phone);
            }
        }));

        alertController.addAction(new BGAAlertAction("Ramstein", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {

                AppUtil.openGoogleMap(activity, "49.446818", "7.601695");


               // Uri webpage = Uri.parse("geo:49.446818,7.601695");
                String strUri = "http://maps.google.com/maps?t=k&q=loc:" + 49.446818 + "," + 7.601695 + " (" + "KEY_KIOSK" + ")";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
          //   Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                startActivity(intent);
            }
        }));
        alertController.setCancelable(false);
        alertController.show();
/*
                    alertController.addAction(new BGAAlertAction("Open Ramstein Map", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
                        @Override
                        public void onClick() {


                            MapsActivity.lat = 49.446818;
                            MapsActivity.lon = 7.601695;
                            MapsActivity.mark_title = "KeyTrak kiosk";
                            Intent intent = new Intent( activity, MapsActivity.class);

                            startActivity(intent);


                        }
                    }));

                    alertController.setCancelable(false);
                    alertController.show();



        alertController.addAction(new BGAAlertAction("Open Kapaun Map", BGAAlertAction.AlertActionStyle.Destructive, new BGAAlertAction.Delegate() {
            @Override
            public void onClick() {


                MapsActivity.lat = 49.446818;
                MapsActivity.lon = 7.601695;
                MapsActivity.mark_title = "KeyTrak kiosk";
                Intent intent = new Intent( activity, MapsActivity.class);
                startActivity(intent);


            }
        }));

        alertController.setCancelable(false);
        alertController.show();
*/

    }

            }









